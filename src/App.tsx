import { Routes, Route } from "react-router-dom";
import "./styles/app.scss";
import Layout from "./pages/Layout";
import Home from "./pages/Home";
import NotFound from "./pages/NotFound";
import ManageTicket from "./pages/ManageTicket";
import PackageTicket from "./pages/PackageTicket";
import VerifyTicket from "./pages/VerifyTicket";

function App() {
  return (
    <Routes>
      <Route path="/" element={<Layout />}>
        <Route index element={<Home />} />
        <Route path="manageticket" element={<ManageTicket />} />
        <Route path="verifyticket" element={<VerifyTicket />} />
        <Route path="serviceticket" element={<PackageTicket />} />
        <Route path="*" element={<NotFound />} />
      </Route>
    </Routes>
  );
}

export default App;
