interface ITicket {
  id: string;
  bookingCode: string;
  ticketCode: string;
  eventName: string;
  status: string;
  dateUse: string;
  dateExport: string;
  gateCheck: number;
  typeTicket: string;
  isChecked: boolean;
}

interface ITicketPackage {
  id: string;
  packageCode: string;
  packageName: string;
  dateApplied: string;
  dateExpired: string;
  priceTicket: number;
  priceCombo: number;
  numTicketCombo: number;
  packageStatus: string;
  eventCode: string;
  eventName: string;
}
