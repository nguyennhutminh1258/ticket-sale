import "./style.scss";
import { ChangeEvent, useEffect, useRef, useState } from "react";
import { AiOutlineCalendar } from "react-icons/ai";
import dayjs from "dayjs";
import Calendar from "../Calendar";
import { useAppDispatch } from "../../hooks/useAppDispatch";
import { getTicketsManageFilter } from "../../redux/slice/ticketManageSlice";
import { setShowFilterModal } from "../../redux/slice/modalSlice";
import { useAppSelector } from "../../hooks/useAppSelector";
import {
  selectFromDateFilterModal,
  selectGateCheckFilterModal,
  selectStatusCheckedFilterModal,
  selectToDateFilterModal,
  setFromDateFilterModal,
  setGateCheckFilterModal,
  setStatusCheckedFilterModal,
  setToDateFilterModal,
} from "../../redux/slice/filterModalSlice";
import { useClickOutside } from "../../hooks/useClickOutside";

interface IGateCheck {
  name: string;
  isCheck: boolean;
  gate: number;
}

const FilterModal = () => {
  const currentDate = dayjs(null);
  const dispatch = useAppDispatch();

  const status = useAppSelector(selectStatusCheckedFilterModal);
  const gateCheck = useAppSelector(selectGateCheckFilterModal);
  const fromDateString = useAppSelector(selectFromDateFilterModal);
  const toDateString = useAppSelector(selectToDateFilterModal);

  const [fromDate, setFromDate] = useState(
    fromDateString !== "" ? dayjs(fromDateString) : currentDate
  );
  const [toDate, setToDate] = useState(toDateString !== "" ? dayjs(toDateString) : currentDate);

  const [showCalendarFrom, setShowCalendarFrom] = useState(false);
  const [showCalendarTo, setShowCalendarTo] = useState(false);

  const calendarFromRef = useRef<any>(null);
  const calendarToRef = useRef<any>(null);

  const handleChangeGateFilter = (postion: number) => {
    if (postion === 0) {
      const newGateCheck: IGateCheck[] = gateCheck.map((gate, index) =>
        index !== 0 ? { ...gate, isCheck: false } : { ...gate, isCheck: true }
      );
      dispatch(setGateCheckFilterModal(newGateCheck));
    } else {
      const newGateCheck: IGateCheck[] = gateCheck.map((gate, index) =>
        index === 0
          ? { ...gate, isCheck: false }
          : index === postion
          ? { ...gate, isCheck: !gate.isCheck }
          : gate
      );
      dispatch(setGateCheckFilterModal(newGateCheck));
    }
  };

  const handleChooseFromDate = (event: ChangeEvent<HTMLInputElement>) => {
    dispatch(setFromDateFilterModal(event.target.value));
    if (dayjs(event.target.value).isValid()) {
      setFromDate(dayjs(event.target.value));
    } else {
      setFromDate(dayjs(null));
    }
  };

  const handleChooseToDate = (event: ChangeEvent<HTMLInputElement>) => {
    dispatch(setToDateFilterModal(event.target.value));
    if (dayjs(event.target.value).isValid()) {
      setToDate(dayjs(event.target.value));
    } else {
      setToDate(dayjs(null));
    }
  };

  const handleChooseStatus = (event: ChangeEvent<HTMLInputElement>) => {
    dispatch(setStatusCheckedFilterModal(event.target.value));
  };

  const handleFilterTicket = () => {
    const filter = {
      status,
      gateList: gateCheck.filter((gate) => gate.isCheck),
      fromDate,
      toDate,
    };
    dispatch(setShowFilterModal(false));
    dispatch(getTicketsManageFilter(filter));
    document.body.style.paddingRight = "0px";
    document.body.style.overflow = "unset";
  };

  useEffect(() => {
    if (gateCheck.every((gate) => !gate.isCheck)) {
      const newGateCheck: IGateCheck[] = gateCheck.map((gate, index) =>
        index === 0 ? { ...gate, isCheck: true } : { ...gate, isCheck: false }
      );
      dispatch(setGateCheckFilterModal(newGateCheck));
    }
    if (
      !gateCheck[0].isCheck &&
      gateCheck[1].isCheck &&
      gateCheck[2].isCheck &&
      gateCheck[3].isCheck &&
      gateCheck[4].isCheck &&
      gateCheck[5].isCheck
    ) {
      const newGateCheck: IGateCheck[] = gateCheck.map((gate, index) =>
        index !== 0 ? { ...gate, isCheck: false } : { ...gate, isCheck: true }
      );
      dispatch(setGateCheckFilterModal(newGateCheck));
    }
  }, [dispatch, gateCheck]);

  useEffect(() => {
    if (fromDate.isValid()) {
      const year = fromDate.toDate().getFullYear();
      if (year > 2000) {
        dispatch(setFromDateFilterModal(fromDate.toDate().toLocaleDateString("en-CA")));
      }
    }
  }, [dispatch, fromDate]);

  useEffect(() => {
    if (toDate.isValid()) {
      const year = toDate.toDate().getFullYear();
      if (year > 2000) {
        dispatch(setToDateFilterModal(toDate.toDate().toLocaleDateString("en-CA")));
      }
    }
  }, [dispatch, toDate]);

  const handleClickCalendarFromOutside = () => {
    setShowCalendarFrom(false);
  };
  const handleClickCalendarToOutside = () => {
    setShowCalendarTo(false);
  };

  useClickOutside([calendarFromRef], handleClickCalendarFromOutside);
  useClickOutside([calendarToRef], handleClickCalendarToOutside);

  return (
    <div className="filter-modal">
      <div className="filter-modal__title">
        <span>Lọc vé</span>
      </div>
      <div className="filter-modal__option">
        <div className="filter-modal__option--date">
          <div className="filter-modal__option--date--from-date" ref={calendarFromRef}>
            <label htmlFor="from-date">Từ ngày</label>
            <div className="choose-date">
              <input
                type="date"
                value={fromDateString}
                onChange={(event) => handleChooseFromDate(event)}

              />
              <div
                className="icon"
                onClick={() => {
                  setShowCalendarFrom(!showCalendarFrom);
                  setShowCalendarTo(false);
                }}>
                <AiOutlineCalendar fontSize={24} color="#FF993C" />
              </div>
            </div>
            {showCalendarFrom && (
              <div className="calendar-menu">
                <Calendar selectedDay={fromDate} setSelectedDay={setFromDate} />
              </div>
            )}
          </div>
          <div className="filter-modal__option--date--to-date" ref={calendarToRef}>
            <label htmlFor="to-date">Đến ngày</label>
            <div className="choose-date">
              <input
                type="date"
                value={toDateString}
                onChange={(event) => handleChooseToDate(event)}
              />
              <div
                className="icon"
                onClick={() => {
                  setShowCalendarFrom(false);
                  setShowCalendarTo(!showCalendarTo);
                }}>
                <AiOutlineCalendar fontSize={24} color="#FF993C" />
              </div>
            </div>
            {showCalendarTo && (
              <div className="calendar-menu">
                <Calendar selectedDay={toDate} setSelectedDay={setToDate} />
              </div>
            )}
          </div>
        </div>
        <div className="filter-modal__option--status">
          <div className="title">
            <span>Tình trạng sử dụng</span>
          </div>
          <div className="wrapper">
            <label className="radio">
              Tất cả
              <input
                type="radio"
                checked={status === "Tất cả"}
                name="status"
                id="all"
                value="Tất cả"
                onChange={(event) => handleChooseStatus(event)}
              />
              <span className="checkmark"></span>
            </label>
            <label className="radio">
              Đã sử dụng
              <input
                type="radio"
                name="status"
                checked={status === "Đã sử dụng"}
                id="used"
                value="Đã sử dụng"
                onChange={(event) => handleChooseStatus(event)}
              />
              <span className="checkmark"></span>
            </label>
            <label className="radio">
              Chưa sử dụng
              <input
                type="radio"
                checked={status === "Chưa sử dụng"}
                name="status"
                id="unused"
                value="Chưa sử dụng"
                onChange={(event) => handleChooseStatus(event)}
              />
              <span className="checkmark"></span>
            </label>
            <label className="radio">
              Hết hạn
              <input
                type="radio"
                checked={status === "Hết hạn"}
                name="status"
                id="expired"
                value="Hết hạn"
                onChange={(event) => handleChooseStatus(event)}
              />
              <span className="checkmark"></span>
            </label>
          </div>
        </div>
        <div className="filter-modal__option--gate">
          <div className="title">
            <span>Cổng Check - in</span>
          </div>
          <div className="wrapper">
            {gateCheck.map((gate, index) => {
              return (
                <label className="checkbox" key={gate.name}>
                  {gate.name}
                  <input
                    type="checkbox"
                    checked={gate.isCheck}
                    name={gate.name}
                    value={gate.name}
                    onChange={() => handleChangeGateFilter(index)}
                  />
                  <span className="checkmark"></span>
                </label>
              );
            })}
          </div>
        </div>
      </div>
      <div className="filter-modal__button">
        <button onClick={handleFilterTicket}>Lọc</button>
      </div>
    </div>
  );
};

export default FilterModal;
