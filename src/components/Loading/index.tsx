import "./style.scss";

const Loading = () => {
  return (
    <div className="loading">
      <span>Đang lấy thông tin vé</span>
    </div>
  );
};

export default Loading;
