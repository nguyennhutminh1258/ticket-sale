import Menu from "../Menu";
import "./style.scss";
import Logo from "../../assets/images/insight-05 1.png";

const Sidebar = () => {
  return (
    <div className="sidebar">
      <div className="sidebar__wrapper-logo">
        <img src={Logo} alt="logo" />
      </div>
      <div className="sidebar__wrapper-menu">
        <Menu />
      </div>
    </div>
  );
};

export default Sidebar;
