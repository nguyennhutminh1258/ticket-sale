import "./style.scss";
import { NavLink } from "react-router-dom";

interface IMenuItemProps {
  title: string;
  icon: string;
  path: string;
}

const MenuItem = ({ title, icon, path }: IMenuItemProps) => {
  return (
    <NavLink className="menu-item" to={path}>
      <div className="menu-item__icon">
        <img src={icon} alt="icon" />
      </div>
      <div className="menu-item__title">{title}</div>
    </NavLink>
  );
};

export default MenuItem;
