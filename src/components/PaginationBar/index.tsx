import "./style.scss";
import { AiFillCaretLeft, AiFillCaretRight } from "react-icons/ai";
import { DOTS, usePagination } from "../../hooks/usePagination";

interface IPaginationBar {
  currentPage: number;
  totalCount: number;
  pageSize: number;
  siblingCount: number;
  onPageChange: (page: number) => void;
}

const PaginationBar = ({
  currentPage,
  totalCount,
  pageSize,
  siblingCount,
  onPageChange,
}: IPaginationBar) => {
  const paginationRange = usePagination({
    currentPage,
    totalCount,
    siblingCount,
    pageSize,
  });

  // If there are less than 2 times in pagination range we shall not render the component
  if (currentPage === 0 || paginationRange!.length < 2) {
    return null;
  }

  const onNext = () => {
    onPageChange(currentPage + 1);
  };

  const onPrevious = () => {
    onPageChange(currentPage - 1);
  };

  // let lastPage = paginationRange?[paginationRange?.length - 1];

  return (
    <div className="pagination-bar">
      <span onClick={onPrevious} className={currentPage === 1 ? "disabled" : ""}>
        <AiFillCaretLeft color={currentPage === 1 ? "#A5A8B1" : "#FF993C"} fontSize={24} />
      </span>
      {paginationRange?.map((pageNumber: any, index) => {
        if (pageNumber === DOTS) {
          return <span key={index + 100}>&#8230;</span>;
        }

        return (
          <span
            key={index + 100}
            className={pageNumber === currentPage ? "active" : ""}
            onClick={() => onPageChange(pageNumber)}>
            {pageNumber}
          </span>
        );
      })}

      <span
        onClick={onNext}
        className={currentPage === paginationRange![paginationRange!.length - 1] ? "disabled" : ""}>
        <AiFillCaretRight
          color={
            currentPage === paginationRange![paginationRange!.length - 1] ? "#A5A8B1" : "#FF993C"
          }
          fontSize={24}
        />
      </span>
    </div>
  );
};

export default PaginationBar;
