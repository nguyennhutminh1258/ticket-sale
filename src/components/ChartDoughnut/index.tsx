import { Doughnut } from "react-chartjs-2";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import "./style.scss";

ChartJS.register(ArcElement, Tooltip, Legend);

const options = {
  plugins: {
    legend: false,
  },
};

interface IChartDoughnutProps {
  dataChart: { label: string; value: number }[];
}

// data [số vé đã sử dụng, số vé chưa sử dụng]

// get data firebase count where status === "Đã sử dụng" and bookingCode === gói sự kiện
// get data firebase count where status === "Chưa sử dụng" and bookingCode === gói sự kiện
// get data firebase count where status === "Đã sử dụng" and bookingCode === gói gia đình
// get data firebase count where status === "Chưa sử dụng" and bookingCode === gói gia đình

const ChartDoughnut = ({ dataChart }: IChartDoughnutProps) => {
  const data = {
    labels: dataChart.map((item) => item.label),
    datasets: [
      {
        data: dataChart.map((item) => item.value),
        backgroundColor: ["#4F75FF", "#FF8A48"],
        borderColor: ["#4F75FF", "#FF8A48"],
        borderWidth: 1,
      },
    ],
  };
  return (
    <div className="chart-dougnnut">
      <Doughnut data={data} options={options as any} />
    </div>
  );
};

export default ChartDoughnut;
