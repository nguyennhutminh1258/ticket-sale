import "./style.scss";
import { NavLink, useLocation } from "react-router-dom";
import MenuItem from "../MenuItem";
import HomeIcon from "../../assets/images/u_home-alt.svg";
import TicketIcon from "../../assets/images/u_ticket.svg";
import InvoiceIcon from "../../assets/images/u_invoice.svg";
import SettingIcon from "../../assets/images/u_setting.svg";

const Menu = () => {
  const { pathname } = useLocation();

  return (
    <div className="menu">
      <MenuItem title="Trang chủ" icon={HomeIcon} path="/" />
      <MenuItem title="Quản lý vé" icon={TicketIcon} path="/manageticket" />
      <MenuItem title="Đối soát vé" icon={InvoiceIcon} path="/verifyticket" />
      <MenuItem title="Cài đặt" icon={SettingIcon} path="/serviceticket" />
      <div
        className={`${
          pathname === "/serviceticket" ? "menu__menu-item-sub active" : "menu__menu-item-sub"
        }`}>
        <NavLink to="/serviceticket" style={{ textDecoration: "none", color: "inherit" }}>
          <span>Gói dịch vụ</span>
        </NavLink>
      </div>
    </div>
  );
};

export default Menu;
