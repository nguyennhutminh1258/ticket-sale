import { useAppDispatch } from "../../hooks/useAppDispatch";
import { setShowAddServiceModal } from "../../redux/slice/modalSlice";
import "./style.scss";

const AddPackageButton = () => {
  const dispatch = useAppDispatch();

  const handleShowAddServiceModal = () => {
    dispatch(setShowAddServiceModal(true));
    document.body.style.paddingRight = "17px";
    document.body.style.overflow = "hidden";
  };

  return (
    <div className="service-btn">
      <button onClick={handleShowAddServiceModal}>
        <span>Thêm gói vé</span>
      </button>
    </div>
  );
};

export default AddPackageButton;
