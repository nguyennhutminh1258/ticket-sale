// import { useState } from "react";
import "./style.scss";
import { useAppSelector } from "../../hooks/useAppSelector";
import {
  selectCurrentPageVerify,
  selectLoadingTicketsVerify,
  selectTicketsVerify,
  setCurrentPageVerify,
} from "../../redux/slice/ticketVerifySlice";
import Loading from "../Loading";
import { useMemo } from "react";
import PaginationBar from "../PaginationBar";
import { useAppDispatch } from "../../hooks/useAppDispatch";

let PageSize = 12;

const TableVerifyTicket = () => {
  const dispatch = useAppDispatch();
  const tickets = useAppSelector(selectTicketsVerify);
  const loading = useAppSelector(selectLoadingTicketsVerify);
  const currentPage = useAppSelector(selectCurrentPageVerify);

  const currentTicketData = useMemo(() => {
    const firstPageIndex = (currentPage - 1) * PageSize;
    const lastPageIndex = firstPageIndex + PageSize;
    return tickets.slice(firstPageIndex, lastPageIndex);
  }, [currentPage, tickets]);

  const handleChangePage = (page: number) => {
    dispatch(setCurrentPageVerify(page));
  };

  return (
    <>
      <table className="table">
        <thead>
          <tr className="header">
            <th>STT</th>
            <th>Số vé</th>
            <th>Ngày sử dụng</th>
            <th>Tên loại vé</th>
            <th>Cổng check - in</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {loading ? (
            <tr>
              <td>
                <Loading />
              </td>
            </tr>
          ) : currentTicketData.length === 0 ? (
            <tr>
              <td>
                <div className="no-data">
                  <span>Không có dữ liệu</span>
                </div>
              </td>
            </tr>
          ) : (
            currentTicketData.map((item, index) => {
              return (
                <tr key={index + 1}>
                  <td>{(currentPage - 1) * PageSize + (index + 1)}</td>
                  <td>{item.ticketCode}</td>
                  <td>{new Date(item.dateUse).toLocaleDateString("en-GB")}</td>
                  <td>{item.typeTicket}</td>
                  <td>Cổng {item.gateCheck}</td>
                  <td>
                    {item.isChecked ? (
                      <span className="checked">Đã đối soát</span>
                    ) : (
                      <span className="unchecked">Chưa đối soát</span>
                    )}
                  </td>
                </tr>
              );
            })
          )}
        </tbody>
      </table>
      <PaginationBar
        currentPage={currentPage}
        totalCount={tickets.length}
        siblingCount={1}
        pageSize={PageSize}
        onPageChange={(page: number) => handleChangePage(page)}
      />
    </>
  );
};

export default TableVerifyTicket;
