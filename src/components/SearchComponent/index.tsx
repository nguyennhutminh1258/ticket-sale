import "./style.scss";
import { AiOutlineSearch } from "react-icons/ai";

const SearchComponent = () => {
  return (
    <div className="search-component">
      <div className="search-component__input">
        <input type="text" placeholder="Search" />
      </div>
      <div className="search-component__icon">
        <AiOutlineSearch fontSize={24} />
      </div>
    </div>
  );
};

export default SearchComponent;
