import "./style.scss";
import { ChangeEvent, useEffect, useRef, useState } from "react";
import { useAppSelector } from "../../hooks/useAppSelector";
import { useAppDispatch } from "../../hooks/useAppDispatch";
import dayjs from "dayjs";
import { MdOutlineCalendarMonth } from "react-icons/md";
import Calendar from "../Calendar";
import { getTicketsVerifyFilter, selectTicketsVerify } from "../../redux/slice/ticketVerifySlice";
import {
  selectEventFilterBar,
  selectFromDateFilterBar,
  selectStatusCheckedFilterBar,
  selectToDateFilterBar,
  setEventFilterBar,
  setFromDateFilterBar,
  setStatusCheckedFilterBar,
  setToDateFilterBar,
} from "../../redux/slice/filterBarSlice";
import useIsFirstRender from "../../hooks/useIsFirstRender";
import { useClickOutside } from "../../hooks/useClickOutside";

const FilterBar = () => {
  const currentDate = dayjs(null);
  const dispatch = useAppDispatch();
  const isFirst = useIsFirstRender();

  const tickets = useAppSelector(selectTicketsVerify);
  const listEvent = new Set(tickets.map((ticket) => ticket.eventName));

  const status = useAppSelector(selectStatusCheckedFilterBar);
  const fromDateString = useAppSelector(selectFromDateFilterBar);
  const toDateString = useAppSelector(selectToDateFilterBar);
  const event = useAppSelector(selectEventFilterBar);

  const [fromDate, setFromDate] = useState(
    fromDateString !== "" && !isFirst ? dayjs(fromDateString) : currentDate
  );
  const [toDate, setToDate] = useState(
    toDateString !== "" && !isFirst ? dayjs(toDateString) : currentDate
  );

  const [showCalendarFrom, setShowCalendarFrom] = useState(false);
  const [showCalendarTo, setShowCalendarTo] = useState(false);

  const calendarFromRef = useRef<any>(null);
  const calendarToRef = useRef<any>(null);

  const handleChooseStatus = (event: ChangeEvent<HTMLInputElement>) => {
    dispatch(setStatusCheckedFilterBar(event.target.value));
  };

  const handleChooseEvent = (event: ChangeEvent<HTMLSelectElement>) => {
    dispatch(setEventFilterBar(event.target.value));
  };

  const handleChooseFromDate = (event: ChangeEvent<HTMLInputElement>) => {
    dispatch(setFromDateFilterBar(event.target.value));
    if (dayjs(event.target.value).isValid()) {
      setFromDate(dayjs(event.target.value));
    } else {
      setFromDate(dayjs(null));
    }
  };

  const handleChooseToDate = (event: ChangeEvent<HTMLInputElement>) => {
    dispatch(setToDateFilterBar(event.target.value));
    if (dayjs(event.target.value).isValid()) {
      setToDate(dayjs(event.target.value));
    } else {
      setToDate(dayjs(null));
    }
  };

  const handleFilterTicket = () => {
    let isChecked;
    if (status !== "All") {
      if (status === "Checked") {
        isChecked = true;
      } else {
        isChecked = false;
      }
    }
    const filter = {
      event,
      isChecked,
      fromDate,
      toDate,
    };
    dispatch(getTicketsVerifyFilter(filter));
  };

  const handleClickCalendarFromOutside = () => {
    setShowCalendarFrom(false);
  };
  const handleClickCalendarToOutside = () => {
    setShowCalendarTo(false);
  };

  useClickOutside([calendarFromRef], handleClickCalendarFromOutside);
  useClickOutside([calendarToRef], handleClickCalendarToOutside);

  useEffect(() => {
    if (fromDate.isValid()) {
      const year = fromDate.toDate().getFullYear();
      if (year > 2000) {
        dispatch(setFromDateFilterBar(fromDate.toDate().toLocaleDateString("en-CA")));
      }
    }
  }, [dispatch, fromDate]);

  useEffect(() => {
    if (toDate.isValid()) {
      const year = toDate.toDate().getFullYear();
      if (year > 2000) {
        dispatch(setToDateFilterBar(toDate.toDate().toLocaleDateString("en-CA")));
      }
    }
  }, [dispatch, toDate]);

  return (
    <div className="filter-bar">
      <div className="filter-bar__title">
        <span>Lọc vé</span>
      </div>
      <div className="filter-bar__option">
        <div className="filter-bar__option--event">
          <select
            name="event"
            id="event"
            value={event}
            onChange={(event) => handleChooseEvent(event)}>
            <option value="Tất cả">Tất cả</option>
            {Array.from(listEvent).map((event) => {
              return (
                <option key={event} value={event}>
                  {event}
                </option>
              );
            })}
          </select>
        </div>
        <div className="filter-bar__option--status">
          <div className="text">
            <span>Tình trạng đối soát</span>
          </div>
          <div className="group-btn">
            <label className="radio">
              <input
                type="radio"
                checked={status === "All"}
                name="status"
                onChange={(event) => handleChooseStatus(event)}
                value="All"
              />
              <span className="checkmark"></span>
              Tất cả
            </label>
            <label className="radio">
              <input
                type="radio"
                checked={status === "Checked"}
                name="status"
                onChange={(event) => handleChooseStatus(event)}
                value="Checked"
              />
              <span className="checkmark"></span>
              Đã đối soát
            </label>
            <label className="radio">
              <input
                type="radio"
                name="status"
                checked={status === "Unchecked"}
                onChange={(event) => handleChooseStatus(event)}
                value="Unchecked"
              />
              <span className="checkmark"></span>
              Chưa đối soát
            </label>
          </div>
        </div>
        <div className="filter-bar__option--type">
          <div className="text">
            <span>Loại vé</span>
          </div>
          <div className="type-ticket">
            <span>Vé cổng</span>
          </div>
        </div>
        <div className="filter-bar__option--from-date">
          <div className="label">
            <span>Từ ngày</span>
          </div>
          <div className="calendar-wrapper" ref={calendarFromRef}>
            <div className="date-text">
              <input
                type="date"
                value={fromDateString}
                onChange={(event) => handleChooseFromDate(event)}
              />
              <div
                className="icon"
                onClick={() => {
                  setShowCalendarFrom(!showCalendarFrom);
                  setShowCalendarTo(false);
                }}>
                <MdOutlineCalendarMonth fontSize={24} color="#A5A8B1" />
              </div>
            </div>
            {showCalendarFrom && (
              <div className="calendar-menu">
                <Calendar selectedDay={fromDate} setSelectedDay={setFromDate} />
              </div>
            )}
          </div>
        </div>
        <div className="filter-bar__option--to-date">
          <div className="label">
            <span>Đến ngày</span>
          </div>
          <div className="calendar-wrapper" ref={calendarToRef}>
            <div className="date-text">
              <input
                type="date"
                value={toDateString}
                onChange={(event) => handleChooseToDate(event)}
              />
              <div
                className="icon"
                onClick={() => {
                  setShowCalendarFrom(false);
                  setShowCalendarTo(!showCalendarTo);
                }}>
                <MdOutlineCalendarMonth fontSize={24} color="#FF993C" />
              </div>
            </div>
            {showCalendarTo && (
              <div className="calendar-menu">
                <Calendar selectedDay={toDate} setSelectedDay={setToDate} />
              </div>
            )}
          </div>
        </div>
      </div>
      <div className="filter-bar__btn">
        <button onClick={handleFilterTicket}>Lọc</button>
      </div>
    </div>
  );
};

export default FilterBar;
