import {
  Chart as ChartJS,
  LineElement,
  CategoryScale,
  LinearScale,
  PointElement,
  Filler,
  ScriptableContext,
} from "chart.js";
import { Line } from "react-chartjs-2";
import { currencyFormat } from "../../helper/currencyFormat";
import { useAppSelector } from "../../hooks/useAppSelector";
import { selectDataChartLine } from "../../redux/slice/chartLineSlice";

ChartJS.register(LineElement, CategoryScale, LinearScale, PointElement, Filler);

const options = {
  plugins: {
    legend: false,
  },
  scales: {
    x: {
      grid: { display: false },
    },
    y: {
      min: 0,
      ticks: {
        callback: (value: number) => currencyFormat(value, 1),
      },
      grid: {
        boderDash: [10],
      },
    },
  },
};

const ChartLine = () => {
  const revenueData = useAppSelector(selectDataChartLine);

  const data = {
    labels: revenueData.map((data) => data.day),
    datasets: [
      {
        data: revenueData.map((data) => data.value),
        borderColor: "#FE8948",
        pointBorderColor: "transparent",
        tension: 0.4,
        fill: true,
        backgroundColor: (context: ScriptableContext<"line">) => {
          const ctx = context.chart.ctx;
          const gradient = ctx.createLinearGradient(0, 0, 0, 300);
          gradient.addColorStop(0, "rgba(250, 160, 95, 0.26)");
          gradient.addColorStop(1, "rgba(255, 255, 255, 0)");
          return gradient;
        },
      },
    ],
  };

  return (
    <div className="chart-line">
      <Line data={data} options={options as any} width={"1200px"} height={"220px"}></Line>
    </div>
  );
};

export default ChartLine;
