import "./style.scss";
import { FiFilter } from "react-icons/fi";
import { useAppDispatch } from "../../hooks/useAppDispatch";
import { setShowFilterModal } from "../../redux/slice/modalSlice";

const FilterButton = () => {
  const dispatch = useAppDispatch();

  const handleOpenFilterModal = () => {
    dispatch(setShowFilterModal(true));
    document.body.style.paddingRight = "17px";
    document.body.style.overflow = "hidden";
  };

  return (
    <div className="filter-button">
      <button onClick={handleOpenFilterModal}>
        <FiFilter fontSize={24} color={"#FF993C"} />
        <span>Lọc vé</span>
      </button>
    </div>
  );
};

export default FilterButton;
