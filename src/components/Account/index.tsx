import "./style.scss";
import MailIcon from "../../assets/images/fi_mail.png";
import BellIcon from "../../assets/images/fi_bell.png";
import UserImage from "../../assets/images/user-image.png";

const Account = () => {
  return (
    <div className="account">
      <div className="account__mail">
        <img src={MailIcon} alt="mail-icon" />
      </div>
      <div className="account__bell">
        <img src={BellIcon} alt="bell-icon" />
      </div>
      <div className="account__user">
        <img src={UserImage} alt="user-img" />
      </div>
    </div>
  );
};

export default Account;
