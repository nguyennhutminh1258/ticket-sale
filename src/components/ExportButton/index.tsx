import "./style.scss";
import { CSVLink } from "react-csv";

type TypeVerifyTicket = {
  stt: number;
  ticketCode: string;
  dateUse: string;
  typeTicket: string;
  gateCheck: string;
};

type TypeManageTicket = {
  stt: number;
  bookingCode: string;
  ticketCode: string;
  eventName: string;
  status: string;
  dateUse: string;
  dateExport: string;
  gateCheck: string;
};

type TypePackageTicket = {
  stt: number;
  packageCode: string;
  packageName: string;
  dateApplied: string;
  dateExpired: string;
  priceTicket: string;
  priceCombo: string;
  packageStatus: string;
};

type TypeData = TypeManageTicket[] | TypeVerifyTicket[] | TypePackageTicket[];

interface IExportButtonProps {
  headers: { label: string; key: string }[];
  data: TypeData;
  fileName: string;
}

const ExportButton = ({ headers, data, fileName }: IExportButtonProps) => {
  return (
    <div className="export-button">
      <button>
        <span>
          <CSVLink
            filename={fileName}
            data={data}
            headers={headers}
            style={{ textDecoration: "none", color: "inherit" }}>
            Xuất file (.csv)
          </CSVLink>
        </span>
      </button>
    </div>
  );
};

export default ExportButton;
