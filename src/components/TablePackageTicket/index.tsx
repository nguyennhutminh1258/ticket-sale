import "./style.scss";
import { useMemo, useState } from "react";
import EditIcon from "../../assets/images/fi_edit.svg";
import { useAppDispatch } from "../../hooks/useAppDispatch";
import {
  selectShowUpdateServiceModal,
  setShowUpdateServiceModal,
} from "../../redux/slice/modalSlice";
import UpdateServiceModal from "../UpdatePackageModal";
import { useAppSelector } from "../../hooks/useAppSelector";
import {
  selectLoadingTicketPackages,
  selectTicketPackages,
} from "../../redux/slice/ticketPackageSlice";
import PaginationBar from "../PaginationBar";
import Loading from "../Loading";

let PageSize = 12;

const TablePackageTicket = () => {
  const dispatch = useAppDispatch();
  const packages = useAppSelector(selectTicketPackages);
  const loading = useAppSelector(selectLoadingTicketPackages);
  const showUpdateServiceModal = useAppSelector(selectShowUpdateServiceModal);
  const [index, setIndex] = useState(0);

  const [currentPage, setCurrentPage] = useState(1);

  const currentPackageData = useMemo(() => {
    const firstPageIndex = (currentPage - 1) * PageSize;
    const lastPageIndex = firstPageIndex + PageSize;
    return packages.slice(firstPageIndex, lastPageIndex);
  }, [currentPage, packages]);

  const handleUpdateService = (index: number) => {
    setIndex(index);
    dispatch(setShowUpdateServiceModal(true));
    document.body.style.paddingRight = "17px";
    document.body.style.overflow = "hidden";
  };

  const handleHideUpdateServiceModal = () => {
    dispatch(setShowUpdateServiceModal(false));
    document.body.style.paddingRight = "0px";
    document.body.style.overflow = "unset";
  };

  return (
    <>
      <table className="table">
        <thead>
          <tr className="header">
            <th>STT</th>
            <th>Mã gói</th>
            <th>Tên gói vé</th>
            <th>Ngày áp dụng</th>
            <th>Ngày hết hạn</th>
            <th>Giá vé (VNĐ/Vé)</th>
            <th>Giá Combo (VNĐ/Combo)</th>
            <th>Tình trạng</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {loading ? (
            <tr>
              <td>
                <Loading />
              </td>
            </tr>
          ) : packages.length === 0 ? (
            <tr>
              <td>
                <div className="no-data">
                  <span>Không có dữ liệu</span>
                </div>
              </td>
            </tr>
          ) : (
            currentPackageData.map((item, index) => {
              const priceStr = new Intl.NumberFormat("en-DE").format(item.priceCombo);
              const combo = priceStr + " VNĐ/" + item.numTicketCombo + " Vé";
              return (
                <tr key={index + 1}>
                  <td>{(currentPage - 1) * PageSize + (index + 1)}</td>
                  <td>{item.packageCode}</td>
                  <td>{item.packageName}</td>
                  <td>
                    <div>{new Date(item.dateApplied).toLocaleDateString("en-GB")}</div>
                    <div>
                      {new Date(item.dateApplied).toLocaleTimeString("en-US", { hour12: false })}
                    </div>
                  </td>
                  <td>
                    <div>{new Date(item.dateExpired).toLocaleDateString("en-GB")}</div>
                    <div>
                      {new Date(item.dateExpired).toLocaleTimeString("en-US", { hour12: false })}
                    </div>
                  </td>
                  <td>{new Intl.NumberFormat("en-DE").format(item.priceTicket)} VNĐ</td>
                  <td>{item.priceCombo !== 0 && combo}</td>
                  <td>
                    <div
                      className={`${
                        item.packageStatus === "Đang áp dụng"
                          ? "applied"
                          : item.packageStatus === "Tắt"
                          ? "off"
                          : ""
                      }`}>
                      <span className="text">
                        <span style={{ fontSize: "40px", marginBottom: "6px" }}>&bull;</span>
                        <span> {item.packageStatus}</span>
                      </span>
                    </div>
                  </td>
                  <td>
                    <div className="update-button">
                      <button onClick={() => handleUpdateService(index)}>
                        <img src={EditIcon} alt="edit-icon" />
                        <span>Cập nhật</span>
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })
          )}
        </tbody>
      </table>
      <PaginationBar
        currentPage={currentPage}
        totalCount={packages.length}
        siblingCount={1}
        pageSize={PageSize}
        onPageChange={(page: number) => setCurrentPage(page)}
      />
      {showUpdateServiceModal && (
        <div className="modal-update-service">
          <div
            className="modal-update-service--outside"
            onClick={handleHideUpdateServiceModal}></div>
          <div className="modal-update-service--update">
            <UpdateServiceModal packageTicket={currentPackageData[index]} />
          </div>
        </div>
      )}
    </>
  );
};

export default TablePackageTicket;
