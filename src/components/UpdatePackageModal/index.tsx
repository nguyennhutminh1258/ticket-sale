import dayjs from "dayjs";
import { ChangeEvent, useRef, useState } from "react";
import { AiOutlineCalendar } from "react-icons/ai";
import { useAppDispatch } from "../../hooks/useAppDispatch";
import { useClickOutside } from "../../hooks/useClickOutside";
import { setShowUpdateServiceModal } from "../../redux/slice/modalSlice";
import { updatePackageTicket } from "../../redux/slice/ticketPackageSlice";
import Calendar from "../Calendar";
import "./style.scss";

interface IUpdateServiceProps {
  packageTicket: ITicketPackage;
}

const UpdatePackageModal = ({ packageTicket }: IUpdateServiceProps) => {
  const dispatch = useAppDispatch();

  let defaultDateApplied = dayjs(packageTicket.dateApplied);
  let defaultDateExpired = dayjs(packageTicket.dateExpired);

  const [eventCode, setEventCode] = useState(packageTicket.eventCode);
  const [eventName, setEventName] = useState(packageTicket.eventName);

  const [dateApplied, setDateApplied] = useState(defaultDateApplied);
  const [dateExpired, setDateExpired] = useState(defaultDateExpired);

  const [showCalendarApplied, setShowCalendarApplied] = useState(false);
  const [showCalendarExpired, setShowCalendarExpired] = useState(false);

  const calendarAppliedRef = useRef<any>(null);
  const calendarExpiredRef = useRef<any>(null);

  const [priceTicket, setPriceTicket] = useState({
    name: "Vé lẻ",
    isCheck: packageTicket.priceTicket !== 0 ? true : false,
    price: packageTicket.priceTicket === 0 ? "" : packageTicket.priceTicket.toString(),
  });
  const [priceCombo, setPriceCombo] = useState({
    name: "Vé combo",
    isCheck: packageTicket.priceCombo !== 0 ? true : false,
    price: packageTicket.priceCombo === 0 ? "" : packageTicket.priceCombo?.toString(),
    number: packageTicket.numTicketCombo === 0 ? "" : packageTicket.numTicketCombo?.toString(),
  });

  const [packageStatus, setPackageStatus] = useState(packageTicket.packageStatus);

  const [error, setError] = useState({ showError: false, msg: "Lỗi" });

  const handleHideUpdateServiceModal = () => {
    dispatch(setShowUpdateServiceModal(false));
    document.body.style.paddingRight = "0px";
    document.body.style.overflow = "unset";
  };

  const onSetDateApplied = (event: ChangeEvent<HTMLInputElement>) => {
    setDateApplied(dayjs(event.target.value));
  };

  const onSetTimeApplied = (event: ChangeEvent<HTMLInputElement>) => {
    const timeFormat = event.target.value.split(":");
    const hour = timeFormat[0];
    const minutes = timeFormat[1];

    const newDate = dayjs(dateApplied).set("hour", parseInt(hour)).set("minute", parseInt(minutes));

    setDateApplied(newDate);
  };

  const onSetDateExpired = (event: ChangeEvent<HTMLInputElement>) => {
    setDateExpired(dayjs(event.target.value));
  };

  const onSetTimeExpired = (event: ChangeEvent<HTMLInputElement>) => {
    const timeFormat = event.target.value.split(":");
    const hour = timeFormat[0];
    const minutes = timeFormat[1];

    const newDate = dayjs(dateExpired).set("hour", parseInt(hour)).set("minute", parseInt(minutes));

    setDateExpired(newDate);
  };

  const handleInputPackage = (event: ChangeEvent<HTMLInputElement>) => {
    if (event.target.name === "event-code") {
      setEventCode(event.target.value);
    } else {
      setEventName(event.target.value);
    }
  };

  const handleChoosePrice = (event: ChangeEvent<HTMLInputElement>) => {
    if (event.target.value === "Vé lẻ") {
      setPriceTicket({ ...priceTicket, isCheck: !priceTicket.isCheck });
    } else {
      setPriceCombo({ ...priceCombo, isCheck: !priceCombo.isCheck });
    }
  };

  const handleInputPrice = (event: ChangeEvent<HTMLInputElement>) => {
    const re = /^[0-9\b]+$/;
    if ((event.target.value === "" || re.test(event.target.value)) && priceTicket.isCheck) {
      setPriceTicket({ ...priceTicket, price: event.target.value });
    }
  };

  const handleInputCombo = (event: ChangeEvent<HTMLInputElement>) => {
    const re = /^[0-9\b]+$/;
    if ((event.target.value === "" || re.test(event.target.value)) && priceCombo.isCheck) {
      setPriceCombo({ ...priceCombo, price: event.target.value });
    }
  };

  const handleInputNumberCombo = (event: ChangeEvent<HTMLInputElement>) => {
    const re = /^[0-9\b]+$/;
    if ((event.target.value === "" || re.test(event.target.value)) && priceCombo.isCheck) {
      setPriceCombo({ ...priceCombo, number: event.target.value });
    }
  };

  const handleChooseStatus = (event: ChangeEvent<HTMLSelectElement>) => {
    setPackageStatus(event.target.value);
  };

  const validateData = () => {
    if (eventCode === "") {
      setError({ showError: true, msg: "Vui lòng nhập mã sự kiện" });
      return false;
    } else if (eventName === "") {
      setError({ showError: true, msg: "Vui lòng nhập tên sự kiện" });
      return false;
    } else if (!dateApplied.isValid()) {
      setError({ showError: true, msg: "Vui lòng chọn ngày áp dụng" });
      return false;
    } else if (!dateExpired.isValid()) {
      setError({ showError: true, msg: "Vui lòng chọn ngày hết hạn" });
      return false;
    } else if (dateExpired.toDate().toISOString < dateApplied.toDate().toISOString) {
      setError({ showError: true, msg: "Vui lòng nhập ngày hết hạn trước ngày áp dụng" });
      return false;
    } else if (!priceTicket.isCheck && !priceCombo.isCheck) {
      setError({ showError: true, msg: "Vui lòng nhập ít nhất một giá vé" });
      return false;
    } else if (priceTicket.isCheck && priceTicket.price === "") {
      setError({ showError: true, msg: "Vui lòng nhập giá vé lẻ" });
      return false;
    } else if (priceCombo.isCheck && (priceCombo.price === "" || priceCombo.number === "")) {
      setError({ showError: true, msg: "Vui lòng nhập giá vé và số vé combo" });
      return false;
    } else {
      setError({ ...error, showError: false });
      return true;
    }
  };

  const handleUpdatePackage = () => {
    if (validateData()) {
      const updatedPackageTicket = {
        id: packageTicket.id,
        dateApplied: dateApplied.toDate().toISOString(),
        dateExpired: dateExpired.toDate().toISOString(),
        eventCode: eventCode,
        eventName: eventName,
        numTicketCombo: parseInt(priceCombo.number!) | 0,
        packageCode: packageTicket.packageCode,
        packageName: packageTicket.packageName,
        packageStatus: packageStatus,
        priceCombo: parseInt(priceCombo.price!) | 0,
        priceTicket: parseInt(priceTicket.price) | 0,
      };
      if (JSON.stringify(updatedPackageTicket) !== JSON.stringify(packageTicket)) {
        dispatch(updatePackageTicket(updatedPackageTicket));
      }
      handleHideUpdateServiceModal();
    }
  };

  const handleClickCalendarAppliedOutside = () => {
    setShowCalendarApplied(false);
  };
  const handleClickCalendarExpiredOutside = () => {
    setShowCalendarExpired(false);
  };

  useClickOutside([calendarAppliedRef], handleClickCalendarAppliedOutside);
  useClickOutside([calendarExpiredRef], handleClickCalendarExpiredOutside);

  return (
    <div className="update-package">
      <div className="update-package__title">Cập nhật thông tin gói vé</div>
      <div className="update-package__info">
        <div className="update-package__info--event">
          <div className="event-code">
            <div className="label-code">
              <label htmlFor="code">Mã sự kiện</label>
              <span className="required">*</span>
            </div>
            <input
              type="text"
              value={eventCode}
              id="code"
              name="event-code"
              onChange={(event) => handleInputPackage(event)}
            />
          </div>
          <div className="event-name">
            <div className="label-name">
              <label htmlFor="name">Tên sự kiện</label>
              <span className="required">*</span>
            </div>
            <input
              type="text"
              value={eventName}
              id="name"
              name="event-name"
              onChange={(event) => handleInputPackage(event)}
            />
          </div>
        </div>
        <div className="update-package__info--date">
          <div className="date-applied">
            <div className="label">
              <span>Ngày áp dụng</span>
            </div>
            <div className="date-input">
              <div className="date-picker__date" ref={calendarAppliedRef}>
                <div className="date">
                  <input
                    type="date"
                    value={dateApplied.toDate().toLocaleDateString("en-CA")}
                    onChange={(event) => onSetDateApplied(event)}
                  />
                  <div
                    className="icon"
                    onClick={() => {
                      setShowCalendarApplied(!showCalendarApplied);
                      setShowCalendarExpired(false);
                    }}>
                    <AiOutlineCalendar fontSize={24} color="#FF993C" />
                  </div>
                </div>
                {showCalendarApplied && (
                  <div className="calendar-menu">
                    <Calendar selectedDay={dateApplied} setSelectedDay={setDateApplied} />
                  </div>
                )}
              </div>
              <div className="date-picker__hour">
                <div className="hour">
                  <input
                    type="time"
                    value={dateApplied.toDate().toLocaleTimeString("en-US", { hour12: false })}
                    onChange={(event) => onSetTimeApplied(event)}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="date-expired">
            <div className="label">
              <span>Ngày hết hạn</span>
            </div>
            <div className="date-input">
              <div className="date-picker__date" ref={calendarExpiredRef}>
                <div className="date">
                  <input
                    type="date"
                    value={dateExpired.toDate().toLocaleDateString("en-CA")}
                    onChange={(event) => onSetDateExpired(event)}
                  />
                  <div
                    className="icon"
                    onClick={() => {
                      setShowCalendarApplied(false);
                      setShowCalendarExpired(!showCalendarExpired);
                    }}>
                    <AiOutlineCalendar fontSize={24} color="#FF993C" />
                  </div>
                </div>
                {showCalendarExpired && (
                  <div className="calendar-menu">
                    <Calendar selectedDay={dateExpired} setSelectedDay={setDateExpired} />
                  </div>
                )}
              </div>
              <div className="date-picker__hour">
                <div className="hour">
                  <input
                    type="time"
                    value={dateExpired.toDate().toLocaleTimeString("en-US", { hour12: false })}
                    onChange={(event) => onSetTimeExpired(event)}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="update-package__info--price">
          <div className="title">
            <span>Giá vé áp dụng</span>
          </div>
          <div className="price-ticket">
            <label className="checkbox">
              <input
                type="checkbox"
                name="check-price"
                id="check-price"
                value={priceTicket.name}
                checked={priceTicket.isCheck}
                onChange={(event) => handleChoosePrice(event)}
              />
              <span className="checkmark"></span>
            </label>
            <span className="text">Vé lẻ (vnđ/vé) với giá </span>
            <div className="show-price">
              <input
                type="text"
                id="input-price"
                placeholder="Giá vé"
                value={priceTicket.price}
                onChange={(event) => handleInputPrice(event)}
              />
              / vé
            </div>
          </div>
          <div className="price-combo">
            <label className="checkbox">
              <input
                type="checkbox"
                name="check-combo"
                id="check-combo"
                value={priceCombo.name}
                checked={priceCombo.isCheck}
                onChange={(event) => handleChoosePrice(event)}
              />
              <span className="checkmark"></span>
            </label>

            <span className="text">Combo vé với giá</span>
            <div className="show-combo">
              <input
                type="text"
                id="input-combo"
                placeholder="Giá vé"
                value={priceCombo.price}
                onChange={(event) => handleInputCombo(event)}
              />
              /
            </div>
            <div className="show-combo-price">
              <input
                type="text"
                id="input-combo-price"
                placeholder="Số vé"
                value={priceCombo.number}
                onChange={(event) => handleInputNumberCombo(event)}
              />
              vé
            </div>
          </div>
        </div>
        <div className="update-package__info--status">
          <label htmlFor="status">Tình trạng</label>

          <select id="status" value={packageStatus} onChange={(event) => handleChooseStatus(event)}>
            <option value="Đang áp dụng">Đang áp dụng</option>
            <option value="Tắt">Tắt</option>
          </select>
        </div>
        <div className="update-package__info--note">
          <span className="start">*</span> <span className="text">là thông tin bắt buộc</span>
        </div>
        {error.showError && (
          <div className={"update-package__info--error"}>
            <span className="text">{error.msg}</span>
          </div>
        )}
      </div>
      <div className="update-package__group-button">
        <button className="btn-cancel" onClick={handleHideUpdateServiceModal}>
          Huỷ
        </button>
        <button className="btn-save" onClick={handleUpdatePackage}>
          Lưu
        </button>
      </div>
    </div>
  );
};

export default UpdatePackageModal;
