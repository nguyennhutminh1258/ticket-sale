import "./style.scss";
import { GrFormNext, GrFormPrevious } from "react-icons/gr";
import { generateDate, months } from "../../helper/calendar";
import dayjs, { Dayjs } from "dayjs";
import { ChangeEvent, Dispatch, SetStateAction, useEffect, useState } from "react";
import { useAppDispatch } from "../../hooks/useAppDispatch";
import { selectChartLineByWeek, setChartLineByWeek } from "../../redux/slice/chartLineSlice";
import { useAppSelector } from "../../hooks/useAppSelector";

interface ICalendarProps {
  selectedDay: Dayjs;
  setSelectedDay: Dispatch<SetStateAction<Dayjs>>;
  chartLine?: boolean;
}

const Calendar = ({ selectedDay, setSelectedDay, chartLine }: ICalendarProps) => {
  const currentDate = dayjs();
  const dispatch = useAppDispatch();

  const chartLineByWeek = useAppSelector(selectChartLineByWeek);

  const days = ["CN", "T2", "T3", "T4", "T5", "T6", "T7"];
  const [baseWeek, setBaseWeek] = useState(false);

  const calendar = generateDate(
    !selectedDay.isValid() ? currentDate.month() : selectedDay.month(),
    !selectedDay.isValid() ? currentDate.year() : selectedDay.year()
  );

  // create array and each array is a set of dates in week
  const weekRows = [...Array(Math.ceil(calendar.length / 7))]?.map((row, index) =>
    calendar?.slice(index * 7, index * 7 + 7)
  );

  const handleChangeDisplay = (event: ChangeEvent<HTMLInputElement>) => {
    if (event.target.value === "date") {
      if (chartLine) {
        dispatch(setChartLineByWeek(false));
      }
      setBaseWeek(false);
    } else {
      if (chartLine) {
        dispatch(setChartLineByWeek(true));
      }
      setBaseWeek(true);
    }
  };

  useEffect(() => {
    console.log(chartLineByWeek);
    if (chartLine) {
      setBaseWeek(chartLineByWeek);
    }
  }, [chartLine, chartLineByWeek]);

  return (
    <div className="calendar">
      <div className="calendar__month-selector">
        <GrFormPrevious
          cursor={"pointer"}
          fontSize={24}
          onClick={() =>
            setSelectedDay(
              !selectedDay.isValid()
                ? currentDate.month(currentDate.month() - 1)
                : selectedDay.month(selectedDay.month() - 1)
            )
          }
        />
        <span className="date">
          {!selectedDay.isValid() ? months[currentDate.month()] : months[selectedDay.month()]},{" "}
          {!selectedDay.isValid() ? currentDate.year() : selectedDay.year()}
        </span>
        <GrFormNext
          cursor={"pointer"}
          fontSize={24}
          onClick={() =>
            setSelectedDay(
              !selectedDay.isValid()
                ? currentDate.month(currentDate.month() + 1)
                : selectedDay.month(selectedDay.month() + 1)
            )
          }
        />
      </div>
      <div className="calendar__option">
        <div className="option-item">
          <label className="radio">
            Theo ngày
            <input
              type="radio"
              checked={chartLine ? !chartLineByWeek : !baseWeek}
              name="date-format"
              id="date"
              value="date"
              onChange={(event) => handleChangeDisplay(event)}
            />
            <span className="checkmark"></span>
          </label>
        </div>
        <div className="option-item">
          <label className="radio">
            Theo tuần
            <input
              type="radio"
              checked={chartLine ? chartLineByWeek : baseWeek}
              name="date-format"
              id="week"
              value="week"
              onChange={(event) => handleChangeDisplay(event)}
            />
            <span className="checkmark"></span>
          </label>
        </div>
      </div>
      <div className="calendar__date">
        <div className="calendar__date--week-days">
          {days.map((day) => {
            return <span key={day}>{day}</span>;
          })}
        </div>
        <div className="calendar__date--days">
          {weekRows.map((week, index) => {
            const currentWeek = week.every((date) => date.dateInWeek);

            return (
              <div
                className={`week${currentWeek && baseWeek ? " current-week" : ""}`}
                key={"week" + index}>
                {week.map(({ dateInWeek, date, currentMonth, today }) => {
                  return (
                    <div
                      className={`${
                        selectedDay.toDate().toDateString() === date.toDate().toDateString()
                          ? "select-date"
                          : dateInWeek && baseWeek
                          ? "date-in-week"
                          : today
                          ? "today"
                          : currentMonth
                          ? "current-month"
                          : "not-current-month"
                      } date`}
                      key={date.format()}
                      onClick={() => setSelectedDay(date)}>
                      <span>{date.date()}</span>
                    </div>
                  );
                })}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Calendar;

// {
//   /* <div
//   className={`${
//     dateInWeek
//       ? "date-in-week"
//       : today
//       ? "today"
//       : currentMonth
//       ? "current-month"
//       : "not-current-month"
//   }`}
//   key={date.format()}
//   onClick={() => setSelectedDay(date)}>
//   <span>{date.date()}</span>
// </div> */
// }
