import "./style.scss";
import { useAppDispatch } from "../../hooks/useAppDispatch";
import { setShowChangeDateModal } from "../../redux/slice/modalSlice";
import { ChangeEvent, useEffect, useRef, useState } from "react";
import dayjs from "dayjs";
import { AiOutlineCalendar } from "react-icons/ai";
import Calendar from "../Calendar";
import { changeDateTicket } from "../../redux/slice/ticketManageSlice";
import { useAppSelector } from "../../hooks/useAppSelector";
import { selectTicketPackages } from "../../redux/slice/ticketPackageSlice";
import { useClickOutside } from "../../hooks/useClickOutside";

interface IChangeDateProps {
  ticket: ITicket;
}

const ChangeDateModal = ({ ticket }: IChangeDateProps) => {
  const dispatch = useAppDispatch();

  let dateTicket = dayjs(ticket.dateUse);

  const packages = useAppSelector(selectTicketPackages);
  const filterPackage = packages.find((item) => item.packageCode === ticket.bookingCode);

  const [date, setDate] = useState(dateTicket);
  const [dateString, setDateString] = useState("");

  const [showCalendar, setShowCalendar] = useState(false);

  const calendarRef = useRef<any>(null);

  const onSetDate = (event: ChangeEvent<HTMLInputElement>) => {
    setDateString(event.target.value);
    if (dayjs(event.target.value).isValid()) {
      setDate(dayjs(event.target.value));
    }
  };

  const handleChangeDate = () => {
    if (dayjs(ticket.dateUse).toDate().toISOString() !== date.toDate().toISOString()) {
      // dispatch change date
      const newTicket = { ...ticket, dateUse: date.toDate().toISOString() };
      dispatch(changeDateTicket(newTicket));
      handleHideChangeDateModal();
    } else {
      handleHideChangeDateModal();
    }
  };

  const handleHideChangeDateModal = () => {
    dispatch(setShowChangeDateModal(false));
    document.body.style.overflow = "unset";
    document.body.style.paddingRight = "0px";
  };

  useEffect(() => {
    if (date.isValid()) {
      const year = date.toDate().getFullYear();
      if (year > 2000) {
        setDateString(date.toDate().toLocaleDateString("en-CA"));
      }
    }
  }, [date]);

  const handleClickCalendarOutside = () => {
    setShowCalendar(false);
  };

  useClickOutside([calendarRef], handleClickCalendarOutside);

  return (
    <div className="change-date">
      <div className="change-date__title">
        <span>Đổi ngày sử dụng vé</span>
      </div>
      <div className="change-date__info">
        <div className="change-date__info--ticket-number">
          <span className="title">Số vé</span>
          <span className="ticket-number">{ticket.ticketCode}</span>
        </div>
        <div className="change-date__info--ticket-type">
          <span className="title">Loại vé</span>
          <span className="ticket-type">
            {ticket.typeTicket} - {filterPackage?.packageName}
          </span>
        </div>
        <div className="change-date__info--event-name">
          <span className="title">Tên sự kiện</span>
          <span className="event-name">{ticket.eventName}</span>
        </div>
        <div className="change-date__info--date">
          <span className="title">Hạn sử dụng</span>
          <div className="date-picker__date" ref={calendarRef}>
            <div className="date">
              <input
                placeholder="dd/mm/yy"
                type="date"
                value={dateString}
                onChange={(event) => onSetDate(event)}
              />
              <div
                className="icon"
                onClick={() => {
                  setShowCalendar(!showCalendar);
                }}>
                <AiOutlineCalendar fontSize={24} color="#FF993C" />
              </div>
            </div>
            {showCalendar && (
              <div className="calendar-menu">
                <Calendar selectedDay={date} setSelectedDay={setDate} />
              </div>
            )}
          </div>
        </div>
      </div>
      <div className="change-date__group-button">
        <button className="btn-cancel" onClick={handleHideChangeDateModal}>
          Huỷ
        </button>
        <button className="btn-save" onClick={handleChangeDate}>
          Lưu
        </button>
      </div>
    </div>
  );
};

export default ChangeDateModal;
