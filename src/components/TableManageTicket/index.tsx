import "./style.scss";
import { MouseEvent, useMemo, useState } from "react";
import { HiEllipsisVertical } from "react-icons/hi2";
import { useAppDispatch } from "../../hooks/useAppDispatch";
import { selectShowChangeDateModal, setShowChangeDateModal } from "../../redux/slice/modalSlice";
import ChangeDateModal from "../ChangeDateModal";
import { useAppSelector } from "../../hooks/useAppSelector";
import {
  selectLoadingTicketsManage,
  selectTicketsManage,
  changeUseTicket,
  setCurrentPageManage,
  selectCurrentPageManage,
} from "../../redux/slice/ticketManageSlice";
import Loading from "../Loading";
import PaginationBar from "../PaginationBar";

let PageSize = 12;

const TableManageTicket = () => {
  const dispatch = useAppDispatch();
  const tickets = useAppSelector(selectTicketsManage);
  const loading = useAppSelector(selectLoadingTicketsManage);
  const showChangeDateModal = useAppSelector(selectShowChangeDateModal);
  const currentPage = useAppSelector(selectCurrentPageManage);

  const [index, setIndex] = useState(0);
  const [left, setLeft] = useState(0);
  const [top, setTop] = useState(0);
  const [showMenu, setShowMenu] = useState(false);

  const currentTicketData = useMemo(() => {
    const firstPageIndex = (currentPage - 1) * PageSize;
    const lastPageIndex = firstPageIndex + PageSize;

    return tickets.slice(firstPageIndex, lastPageIndex);
  }, [currentPage, tickets]);

  const handleOpenMenu = (event: MouseEvent<SVGElement>) => {
    if (!showMenu) {
      setTop(event.pageY + 20);
      setLeft(event.pageX - 120);
      setShowMenu(true);
    } else {
      setShowMenu(false);
    }
  };

  const handleHideChangeDateModal = () => {
    dispatch(setShowChangeDateModal(false));
    document.body.style.paddingRight = "0px";
    document.body.style.overflow = "unset";
  };

  const handleShowChangeDateModal = () => {
    dispatch(setShowChangeDateModal(true));
    setShowMenu(false);
    document.body.style.paddingRight = "17px";
    document.body.style.overflow = "hidden";
  };

  const handleUseTicket = () => {
    dispatch(changeUseTicket(tickets[index].id));
    setShowMenu(false);
  };

  const handleChangePage = (page: number) => {
    dispatch(setCurrentPageManage(page));
  };

  return (
    <>
      <table className="table">
        <thead>
          <tr className="header">
            <th>STT</th>
            <th>Booking code</th>
            <th>Số vé</th>
            <th>Tên sự kiện</th>
            <th>Tình trạng sử dụng</th>
            <th>Ngày sử dụng</th>
            <th>Ngày xuất vé</th>
            <th>Cổng check - in</th>
            <th></th>
          </tr>
        </thead>

        <tbody>
          {loading ? (
            <tr>
              <td>
                <Loading />
              </td>
            </tr>
          ) : currentTicketData.length === 0 ? (
            <tr>
              <td>
                <div className="no-data">
                  <span>Không có dữ liệu</span>
                </div>
              </td>
            </tr>
          ) : (
            currentTicketData.map((item, index) => {
              return (
                <tr key={item.id}>
                  <td>{(currentPage - 1) * PageSize + (index + 1)}</td>
                  <td>{item.bookingCode}</td>
                  <td>{item.ticketCode}</td>
                  <td>{item.eventName}</td>
                  <td>
                    <div
                      className={`${
                        item.status === "Đã sử dụng"
                          ? "used"
                          : item.status === "Chưa sử dụng"
                          ? "not-used"
                          : item.status === "Hết hạn"
                          ? "expired"
                          : ""
                      }`}>
                      <span className="text">
                        <span style={{ fontSize: "40px", marginBottom: "6px" }}>&bull;</span>
                        <span> {item.status}</span>
                      </span>
                    </div>
                  </td>
                  <td>{new Date(item.dateUse).toLocaleDateString("en-GB")}</td>
                  <td>{new Date(item.dateExport).toLocaleDateString("en-GB")}</td>
                  <td>Cổng {item.gateCheck}</td>
                  <td>
                    {item.status === "Chưa sử dụng" && (
                      <HiEllipsisVertical
                        className="table-icon"
                        fontSize={24}
                        color="#1E0D03"
                        onClick={(event) => {
                          setIndex(index);
                          handleOpenMenu(event);
                        }}
                      />
                    )}
                  </td>
                </tr>
              );
            })
          )}
        </tbody>
      </table>

      <PaginationBar
        currentPage={currentPage}
        totalCount={tickets.length}
        siblingCount={1}
        pageSize={PageSize}
        onPageChange={(page: number) => handleChangePage(page)}
      />

      {showMenu && currentTicketData[index].status === "Chưa sử dụng" && (
        <div style={{ top: top, left: left }} className="menu-dropdown">
          <div className="menu-dropdown__item" onClick={handleShowChangeDateModal}>
            <span>Đổi ngày sử dụng</span>
          </div>

          <div className="menu-dropdown__item" onClick={handleUseTicket}>
            <span>Sử dụng vé</span>
          </div>
        </div>
      )}
      {showChangeDateModal && (
        <div className="modal-change-date">
          <div className="modal-change-date--outside" onClick={handleHideChangeDateModal}></div>
          <div className="modal-change-date--date">
            <ChangeDateModal ticket={currentTicketData[index]} />
          </div>
        </div>
      )}
    </>
  );
};

export default TableManageTicket;
