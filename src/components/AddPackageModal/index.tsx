import dayjs from "dayjs";
import { ChangeEvent, useEffect, useRef, useState } from "react";
import { AiOutlineCalendar } from "react-icons/ai";
import { useAppDispatch } from "../../hooks/useAppDispatch";
import { useClickOutside } from "../../hooks/useClickOutside";
import { setShowAddServiceModal } from "../../redux/slice/modalSlice";
import { addPackageTicket } from "../../redux/slice/ticketPackageSlice";
import Calendar from "../Calendar";
import "./style.scss";

const AddPackageModal = () => {
  const dispatch = useAppDispatch();
  const currentDate = dayjs(null);

  const packageCodeRef = useRef<HTMLInputElement>(null);
  const packageNameRef = useRef<HTMLInputElement>(null);

  const [dateApplied, setDateApplied] = useState(currentDate);
  const [dateAppliedString, setDateAppliedString] = useState("");
  const [dateAppliedTime, setDateAppliedTime] = useState("");

  const [dateExpired, setDateExpired] = useState(currentDate);
  const [dateExpiredString, setDateExpiredString] = useState("");
  const [dateExpiredTime, setDateExpiredTime] = useState("");

  const [showCalendarApplied, setShowCalendarApplied] = useState(false);
  const [showCalendarExpired, setShowCalendarExpired] = useState(false);

  const calendarAppliedRef = useRef<any>(null);
  const calendarExpiredRef = useRef<any>(null);

  const [priceTicket, setPriceTicket] = useState({ name: "Vé lẻ", isCheck: true, price: "" });
  const [priceCombo, setPriceCombo] = useState({
    name: "Vé combo",
    isCheck: false,
    price: "",
    number: "",
  });

  const [packageStatus, setPackageStatus] = useState("Đang áp dụng");

  const [error, setError] = useState({ showError: false, msg: "Lỗi" });

  const onSetDateApplied = (event: ChangeEvent<HTMLInputElement>) => {
    setDateAppliedString(event.target.value);
    if (dayjs(event.target.value).isValid()) {
      setDateApplied(dayjs(event.target.value));
    }
  };

  const onSetTimeApplied = (event: ChangeEvent<HTMLInputElement>) => {
    setDateAppliedTime(event.target.value);
    const timeFormat = event.target.value.split(":");
    const hour = timeFormat[0];
    const minutes = timeFormat[1];

    const newDate = dayjs(dateApplied).set("hour", parseInt(hour)).set("minute", parseInt(minutes));
    setDateApplied(newDate);
  };

  const onSetDateExpired = (event: ChangeEvent<HTMLInputElement>) => {
    setDateExpiredString(event.target.value);
    if (dayjs(event.target.value).isValid()) {
      setDateExpired(dayjs(event.target.value));
    }
  };

  const onSetTimeExpired = (event: ChangeEvent<HTMLInputElement>) => {
    setDateExpiredTime(event.target.value);
    const timeFormat = event.target.value.split(":");
    const hour = timeFormat[0];
    const minutes = timeFormat[1];

    const newDate = dayjs(dateExpired).set("hour", parseInt(hour)).set("minute", parseInt(minutes));

    setDateExpired(newDate);
  };

  const handleChoosePrice = (event: ChangeEvent<HTMLInputElement>) => {
    if (event.target.value === "Vé lẻ") {
      setPriceTicket({ ...priceTicket, isCheck: !priceTicket.isCheck });
    } else {
      setPriceCombo({ ...priceCombo, isCheck: !priceCombo.isCheck });
    }
  };

  const handleInputPrice = (event: ChangeEvent<HTMLInputElement>) => {
    const re = /^[0-9\b]+$/;
    if ((event.target.value === "" || re.test(event.target.value)) && priceTicket.isCheck) {
      setPriceTicket({ ...priceTicket, price: event.target.value });
    }
  };

  const handleInputCombo = (event: ChangeEvent<HTMLInputElement>) => {
    const re = /^[0-9\b]+$/;
    if ((event.target.value === "" || re.test(event.target.value)) && priceCombo.isCheck) {
      setPriceCombo({ ...priceCombo, price: event.target.value });
    }
  };

  const handleInputNumberCombo = (event: ChangeEvent<HTMLInputElement>) => {
    const re = /^[0-9\b]+$/;
    if ((event.target.value === "" || re.test(event.target.value)) && priceCombo.isCheck) {
      setPriceCombo({ ...priceCombo, number: event.target.value });
    }
  };

  const handleChooseStatus = (event: ChangeEvent<HTMLSelectElement>) => {
    setPackageStatus(event.target.value);
  };

  const validateData = () => {
    if (packageCodeRef.current?.value === "") {
      setError({ showError: true, msg: "Vui lòng nhập mã gói vé" });
      return false;
    } else if (packageNameRef.current?.value === "") {
      setError({ showError: true, msg: "Vui lòng nhập tên gói vé" });
      return false;
    } else if (!dateApplied.isValid()) {
      setError({ showError: true, msg: "Vui lòng chọn ngày áp dụng" });
      return false;
    } else if (!dateExpired.isValid()) {
      setError({ showError: true, msg: "Vui lòng chọn ngày hết hạn" });
      return false;
    } else if (dateExpired.toDate().toISOString < dateApplied.toDate().toISOString) {
      setError({ showError: true, msg: "Vui lòng nhập ngày hết hạn trước ngày áp dụng" });
      return false;
    } else if (!priceTicket.isCheck && !priceCombo.isCheck) {
      setError({ showError: true, msg: "Vui lòng nhập ít nhất một giá vé" });
      return false;
    } else if (priceTicket.isCheck && priceTicket.price === "") {
      setError({ showError: true, msg: "Vui lòng nhập giá vé lẻ" });
      return false;
    } else if (priceCombo.isCheck && (priceCombo.price === "" || priceCombo.number === "")) {
      setError({ showError: true, msg: "Vui lòng nhập giá vé và số vé combo" });
      return false;
    } else {
      setError({ ...error, showError: false });
      return true;
    }
  };

  const handleAddPackage = () => {
    if (validateData()) {
      const packageTicket = {
        dateApplied: dateApplied.toDate().toISOString(),
        dateExpired: dateExpired.toDate().toISOString(),
        eventCode: "",
        eventName: "",
        numTicketCombo: parseInt(priceCombo.number) | 0,
        packageCode: packageCodeRef.current?.value,
        packageName: packageNameRef.current?.value,
        packageStatus: packageStatus,
        priceCombo: parseInt(priceCombo.price) | 0,
        priceTicket: parseInt(priceTicket.price) | 0,
      };
      dispatch(addPackageTicket(packageTicket));
      handleHideAddPackageModal();
    }
  };

  const handleHideAddPackageModal = () => {
    dispatch(setShowAddServiceModal(false));
    document.body.style.paddingRight = "0px";
    document.body.style.overflow = "unset";
  };

  useEffect(() => {
    if (dateApplied.isValid()) {
      const year = dateApplied.toDate().getFullYear();
      if (year > 2000) {
        setDateAppliedString(dateApplied.toDate().toLocaleDateString("en-CA"));
      }
    }
  }, [dateApplied]);

  useEffect(() => {
    if (dateExpired.isValid()) {
      const year = dateExpired.toDate().getFullYear();
      if (year > 2000) {
        setDateExpiredString(dateExpired.toDate().toLocaleDateString("en-CA"));
      }
    }
  }, [dateExpired]);

  const handleClickCalendarAppliedOutside = () => {
    setShowCalendarApplied(false);
  };
  const handleClickCalendarExpiredOutside = () => {
    setShowCalendarExpired(false);
  };

  useClickOutside([calendarAppliedRef], handleClickCalendarAppliedOutside);
  useClickOutside([calendarExpiredRef], handleClickCalendarExpiredOutside);

  return (
    <div className="add-package">
      <div className="add-package__title">
        <span>Thêm gói vé</span>
      </div>
      <div className="add-package__info">
        <div className="add-package__info--package">
          <div className="package-code">
            <div className="label-code">
              <label htmlFor="code">Mã gói vé</label>
              <span className="required">*</span>
            </div>
            <input type="text" id="code" placeholder="Nhập mã gói vé" ref={packageCodeRef} />
          </div>
          <div className="package-name">
            <div className="label-name">
              <label htmlFor="name">Tên gói vé</label>
              <span className="required">*</span>
            </div>
            <input type="text" id="name" placeholder="Nhập tên gói vé" ref={packageNameRef} />
          </div>
        </div>
        <div className="add-package__info--date">
          <div className="date-applied">
            <div className="label">
              <span>Ngày áp dụng</span>
            </div>
            <div className="date-input">
              <div className="date-picker__date" ref={calendarAppliedRef}>
                <div className="date">
                  <input
                    type="date"
                    value={dateAppliedString}
                    onChange={(event) => onSetDateApplied(event)}
                  />
                  <div
                    className="icon"
                    onClick={() => {
                      setShowCalendarApplied(!showCalendarApplied);
                      setShowCalendarExpired(false);
                    }}>
                    <AiOutlineCalendar fontSize={24} color="#FF993C" />
                  </div>
                </div>
                {showCalendarApplied && (
                  <div className="calendar-menu">
                    <Calendar selectedDay={dateApplied} setSelectedDay={setDateApplied} />
                  </div>
                )}
              </div>
              <div className="date-picker__hour">
                <div className="hour">
                  <input
                    type="time"
                    value={dateAppliedTime}
                    onChange={(event) => onSetTimeApplied(event)}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="date-expired">
            <div className="label">
              <span>Ngày hết hạn</span>
            </div>
            <div className="date-input">
              <div className="date-picker__date" ref={calendarExpiredRef}>
                <div className="date">
                  <input
                    type="date"
                    value={dateExpiredString}
                    onChange={(event) => onSetDateExpired(event)}
                  />
                  <div
                    className="icon"
                    onClick={() => {
                      setShowCalendarApplied(false);
                      setShowCalendarExpired(!showCalendarExpired);
                    }}>
                    <AiOutlineCalendar fontSize={24} color="#FF993C" />
                  </div>
                </div>
                {showCalendarExpired && (
                  <div className="calendar-menu">
                    <Calendar selectedDay={dateExpired} setSelectedDay={setDateExpired} />
                  </div>
                )}
              </div>
              <div className="date-picker__hour">
                <div className="hour">
                  <input
                    type="time"
                    value={dateExpiredTime}
                    onChange={(event) => onSetTimeExpired(event)}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="add-package__info--price">
          <div className="title">
            <span>Giá vé áp dụng</span>
          </div>
          <div className="price-ticket">
            <label className="checkbox">
              <input
                type="checkbox"
                name="check-price"
                id="check-price"
                value={priceTicket.name}
                checked={priceTicket.isCheck}
                onChange={(event) => handleChoosePrice(event)}
              />
              <span className="checkmark"></span>
            </label>

            <span className="text">Vé lẻ (vnđ/vé) với giá </span>
            <div className="show-price">
              <input
                type="text"
                id="input-price"
                placeholder="Giá vé"
                value={priceTicket.price}
                onChange={(event) => handleInputPrice(event)}
              />
              / vé
            </div>
          </div>
          <div className="price-combo">
            <label className="checkbox">
              <input
                type="checkbox"
                name="check-combo"
                id="check-combo"
                value={priceCombo.name}
                checked={priceCombo.isCheck}
                onChange={(event) => handleChoosePrice(event)}
              />
              <span className="checkmark"></span>
            </label>

            <span className="text">Combo vé với giá</span>
            <div className="show-combo">
              <input
                type="text"
                id="input-combo-price"
                placeholder="Giá vé"
                value={priceCombo.price}
                onChange={(event) => handleInputCombo(event)}
              />{" "}
              /
            </div>
            <div className="show-combo-price">
              <input
                type="text"
                id="input-combo"
                placeholder="Số vé"
                value={priceCombo.number}
                onChange={(event) => handleInputNumberCombo(event)}
              />{" "}
              vé
            </div>
          </div>
        </div>
        <div className="add-package__info--status">
          <label htmlFor="status">Tình trạng</label>

          <select id="status" value={packageStatus} onChange={(event) => handleChooseStatus(event)}>
            <option value="Đang áp dụng">Đang áp dụng</option>
            <option value="Tắt">Tắt</option>
          </select>
        </div>
        <div className="add-package__info--note">
          <span className="start">*</span> <span className="text">là thông tin bắt buộc</span>
        </div>
        {error.showError && (
          <div className={"add-package__info--error"}>
            <span className="text">{error.msg}</span>
          </div>
        )}
      </div>
      <div className="add-package__group-button">
        <button className="btn-cancel" onClick={handleHideAddPackageModal}>
          Huỷ
        </button>
        <button className="btn-save" onClick={handleAddPackage}>
          Lưu
        </button>
      </div>
    </div>
  );
};

export default AddPackageModal;
