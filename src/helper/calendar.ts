import dayjs, { Dayjs } from "dayjs";

export const generateDate = (month = dayjs().month(), year = dayjs().year()) => {
  const startWeek = dayjs().year(year).month(month).startOf("week");
  const endWeek = dayjs().year(year).month(month).endOf("week");

  const firstDayOfMonth = dayjs().year(year).month(month).startOf("month");
  const lastDayOfMonth = dayjs().year(year).month(month).endOf("month");

  const arrayOfDate: {
    currentMonth: boolean;
    date: Dayjs;
    today?: boolean;
    dateInWeek?: boolean;
  }[] = [];

  //create prefix date .day() => get day of week
  for (let i = 0; i < firstDayOfMonth.day(); i++) {
    arrayOfDate.push({
      currentMonth: false,
      date: firstDayOfMonth.day(i),
      dateInWeek:
        firstDayOfMonth.date(i).toDate().toDateString() === startWeek.toDate().toDateString() ||
        firstDayOfMonth.date(i).toDate().toDateString() === endWeek.toDate().toDateString() ||
        (firstDayOfMonth.date(i).toDate() > startWeek.toDate() &&
          firstDayOfMonth.date(i).toDate() < endWeek.toDate()),
    });
  }

  //generate current date .date() => get day of month
  for (let i = firstDayOfMonth.date(); i <= lastDayOfMonth.date(); i++) {
    arrayOfDate.push({
      currentMonth: true,
      date: firstDayOfMonth.date(i),
      today: firstDayOfMonth.date(i).toDate().toDateString() === dayjs().toDate().toDateString(),
      dateInWeek:
        firstDayOfMonth.date(i).toDate().toDateString() === startWeek.toDate().toDateString() ||
        firstDayOfMonth.date(i).toDate().toDateString() === endWeek.toDate().toDateString() ||
        (firstDayOfMonth.date(i).toDate() > startWeek.toDate() &&
          firstDayOfMonth.date(i).toDate() < endWeek.toDate()),
    });
  }

  const remaining = 42 - arrayOfDate.length;
  for (let i = lastDayOfMonth.date() + 1; i < lastDayOfMonth.date() + remaining + 1; i++) {
    arrayOfDate.push({
      currentMonth: false,
      date: firstDayOfMonth.date(i),
      dateInWeek:
        firstDayOfMonth.date(i).toDate().toDateString() === startWeek.toDate().toDateString() ||
        firstDayOfMonth.date(i).toDate().toDateString() === endWeek.toDate().toDateString() ||
        (firstDayOfMonth.date(i).toDate() > startWeek.toDate() &&
          firstDayOfMonth.date(i).toDate() < endWeek.toDate()),
    });
  }

  return arrayOfDate;
};

export const months = [
  "Tháng 1",
  "Tháng 2",
  "Tháng 3",
  "Tháng 4",
  "Tháng 5",
  "Tháng 6",
  "Tháng 7",
  "Tháng 8",
  "Tháng 9",
  "Tháng 10",
  "Tháng 11",
  "Tháng 12",
];

export const weekday = ["CN", "Thứ 2", "Thứ 3", "Thứ 4", "Thứ 5", "Thứ 6", "Thứ 7"];
