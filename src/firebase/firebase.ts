// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAHhcqE7qEQu4SVLW4XwOZA9KxghuTt0cs",
  authDomain: "cms-ticket-sale-f06eb.firebaseapp.com",
  projectId: "cms-ticket-sale-f06eb",
  storageBucket: "cms-ticket-sale-f06eb.appspot.com",
  messagingSenderId: "643465968156",
  appId: "1:643465968156:web:5f1e619a7ce3de6ffd7dd9",
  measurementId: "G-4QWL098L27",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// Initialize Cloud Firestore and get a reference to the service
export const db = getFirestore(app);
