import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import type { RootState } from "../configureStore";
import { collection, doc, getDocs, orderBy, query, updateDoc, where } from "firebase/firestore";
import { db } from "../../firebase/firebase";
import { Dayjs } from "dayjs";

// Define a type for the slice state
interface TicketManageState {
  tickets: ITicket[];
  loading: boolean;
  currentPage: number;
}

// Define the initial state using that type
const initialState: TicketManageState = {
  tickets: [],
  loading: false,
  currentPage: 1,
};

interface FilterType {
  status: string;
  gateList: {
    name: string;
    isCheck: boolean;
    gate: number;
  }[];
  fromDate: Dayjs;
  toDate: Dayjs;
}

export const ticketManageSlice = createSlice({
  name: "ticketManage",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    setTicketsManage: (state, action: PayloadAction<ITicket[]>) => {
      state.tickets = action.payload;
    },
    setCurrentPageManage: (state, action: PayloadAction<number>) => {
      state.currentPage = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getTicketsManage.pending, (state) => {
        state.loading = true;
      })
      .addCase(getTicketsManage.fulfilled, (state, action) => {
        state.tickets = action.payload;
        state.currentPage = 1;
        state.loading = false;
      })
      .addCase(getTicketsManage.rejected, (state) => {
        state.tickets = [];
        state.loading = false;
      })
      .addCase(getTicketsManageFilter.pending, (state) => {
        state.loading = true;
        state.currentPage = 1;
      })
      .addCase(getTicketsManageFilter.fulfilled, (state, action) => {
        state.tickets = action.payload;
        state.loading = false;
      })
      .addCase(getTicketsManageFilter.rejected, (state) => {
        state.tickets = [];
        state.loading = false;
      })
      .addCase(searchTicketsManage.pending, (state) => {
        state.loading = true;
        state.currentPage = 1;
      })
      .addCase(searchTicketsManage.fulfilled, (state, action) => {
        state.tickets = action.payload;
        state.loading = false;
      })
      .addCase(searchTicketsManage.rejected, (state) => {
        state.tickets = [];
        state.loading = false;
      })
      .addCase(changeDateTicket.fulfilled, (state, action) => {
        state.tickets = state.tickets.map((item) =>
          item.id === action.payload.id ? action.payload : item
        );
      })
      .addCase(changeUseTicket.fulfilled, (state, action) => {
        state.tickets = state.tickets.map((item) =>
          item.id === action.payload ? { ...item, status: "Đã sử dụng", isChecked: true } : item
        );
      });
  },
});

export const { setTicketsManage, setCurrentPageManage } = ticketManageSlice.actions;

// // Other code such as selectors can use the imported `RootState` type
export const selectTicketsManage = (state: RootState) => state.ticketManage.tickets;
export const selectLoadingTicketsManage = (state: RootState) => state.ticketManage.loading;
export const selectCurrentPageManage = (state: RootState) => state.ticketManage.currentPage;

export const getTicketsManage = createAsyncThunk("ticketManage/getTicketsManage", async () => {
  const ticketRef = collection(db, "ticket");
  const q = query(ticketRef, orderBy("dateUse", "asc"));
  const docsSnap = await getDocs(q);
  const tickets: ITicket[] = [];

  docsSnap.forEach((docItem) => {
    tickets.push({
      id: docItem.id,
      bookingCode: docItem.data().bookingCode,
      ticketCode: docItem.data().ticketCode,
      eventName: docItem.data().eventName,
      status: docItem.data().status,
      dateUse: docItem.data().dateUse,
      dateExport: docItem.data().dateExport,
      gateCheck: docItem.data().gateCheck,
      typeTicket: docItem.data().typeTicket,
      isChecked: docItem.data().isChecked,
    });
  });

  return tickets;
});

export const getTicketsManageFilter = createAsyncThunk(
  "ticketManage/getTicketsManageFilter",
  async (filter: FilterType) => {
    const ticketRef = collection(db, "ticket");
    const queryConstraints = [];

    if (filter.status !== "Tất cả") {
      queryConstraints.push(where("status", "==", filter.status));
    }
    if (filter.fromDate.isValid()) {
      queryConstraints.push(where("dateUse", ">=", filter.fromDate.toDate().toISOString()));
    }
    if (filter.toDate.isValid()) {
      queryConstraints.push(where("dateUse", "<=", filter.toDate.toDate().toISOString()));
    }
    if (filter.gateList[0].gate !== 0) {
      queryConstraints.push(
        where(
          "gateCheck",
          "in",
          filter.gateList.map((item) => item.gate)
        )
      );
    }

    const q = query(ticketRef, ...queryConstraints, orderBy("dateUse", "asc"));
    const tickets: ITicket[] = [];

    const docsSnap = await getDocs(q);
    docsSnap.forEach((docItem) => {
      tickets.push({
        id: docItem.id,
        bookingCode: docItem.data().bookingCode,
        ticketCode: docItem.data().ticketCode,
        eventName: docItem.data().eventName,
        status: docItem.data().status,
        dateUse: docItem.data().dateUse,
        dateExport: docItem.data().dateExport,
        gateCheck: docItem.data().gateCheck,
        typeTicket: docItem.data().typeTicket,
        isChecked: docItem.data().isChecked,
      });
    });

    return tickets;
  }
);

export const searchTicketsManage = createAsyncThunk(
  "ticketManage/searchTicketsManage",
  async (searchString: string | undefined) => {
    const ticketRef = collection(db, "ticket");
    const queryConstraints = [];
    if (searchString !== "") {
      queryConstraints.push(where("ticketCode", ">=", searchString));
      queryConstraints.push(where("ticketCode", "<=", searchString + "\uf8ff"));
    } else {
      queryConstraints.push(orderBy("dateUse", "asc"));
    }
    const q = query(ticketRef, ...queryConstraints);
    const tickets: ITicket[] = [];

    const docsSnap = await getDocs(q);
    docsSnap.forEach((docItem) => {
      tickets.push({
        id: docItem.id,
        bookingCode: docItem.data().bookingCode,
        ticketCode: docItem.data().ticketCode,
        eventName: docItem.data().eventName,
        status: docItem.data().status,
        dateUse: docItem.data().dateUse,
        dateExport: docItem.data().dateExport,
        gateCheck: docItem.data().gateCheck,
        typeTicket: docItem.data().typeTicket,
        isChecked: docItem.data().isChecked,
      });
    });

    return tickets;
  }
);

export const changeDateTicket = createAsyncThunk(
  "ticketManage/changeDateTicket",
  async (newTicket: ITicket) => {
    await updateDoc(doc(db, "ticket", newTicket.id), {
      dateUse: newTicket.dateUse,
    });
    return newTicket;
  }
);

export const changeUseTicket = createAsyncThunk(
  "ticketManage/changeUseTicket",
  async (idTicket: string) => {
    await updateDoc(doc(db, "ticket", idTicket), {
      status: "Đã sử dụng",
      isChecked: true,
    });
    return idTicket;
  }
);

export default ticketManageSlice.reducer;
