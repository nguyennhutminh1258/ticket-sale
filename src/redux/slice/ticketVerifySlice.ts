import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import type { RootState } from "../configureStore";
import { collection, getDocs, orderBy, query, where } from "firebase/firestore";
import { db } from "../../firebase/firebase";
import { Dayjs } from "dayjs";

// Define a type for the slice state
interface TicketVerifyState {
  tickets: ITicket[];
  loading: boolean;
  currentPage: number;
}

interface FilterType {
  event: string;
  isChecked: boolean | undefined;
  fromDate: Dayjs;
  toDate: Dayjs;
}

// Define the initial state using that type
const initialState: TicketVerifyState = {
  tickets: [],
  loading: false,
  currentPage: 1,
};

export const ticketVerifySlice = createSlice({
  name: "ticketVerify",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    setTicketsVerify: (state, action: PayloadAction<ITicket[]>) => {
      state.tickets = action.payload;
    },
    setCurrentPageVerify: (state, action: PayloadAction<number>) => {
      state.currentPage = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getTicketsVerify.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(getTicketsVerify.fulfilled, (state, action) => {
        state.tickets = action.payload;
        state.currentPage = 1;
        state.loading = false;
      })
      .addCase(getTicketsVerifyFilter.pending, (state) => {
        state.loading = true;
        state.currentPage = 1;
      })
      .addCase(getTicketsVerifyFilter.fulfilled, (state, action) => {
        state.tickets = action.payload;
        state.loading = false;
      })
      .addCase(getTicketsVerifyFilter.rejected, (state) => {
        state.tickets = [];
        state.loading = false;
      })
      .addCase(searchTicketsVerify.pending, (state) => {
        state.loading = true;
        state.currentPage = 1;
      })
      .addCase(searchTicketsVerify.fulfilled, (state, action) => {
        state.tickets = action.payload;
        state.loading = false;
      })
      .addCase(searchTicketsVerify.rejected, (state) => {
        state.tickets = [];
        state.loading = false;
      });
  },
});

export const { setTicketsVerify, setCurrentPageVerify } = ticketVerifySlice.actions;

// // Other code such as selectors can use the imported `RootState` type
export const selectTicketsVerify = (state: RootState) => state.ticketVerify.tickets;
export const selectLoadingTicketsVerify = (state: RootState) => state.ticketVerify.loading;
export const selectCurrentPageVerify = (state: RootState) => state.ticketVerify.currentPage;

export const getTicketsVerify = createAsyncThunk("ticketVerify/getTicketsVerify", async () => {
  const ticketRef = collection(db, "ticket");
  const q = query(ticketRef, orderBy("dateUse", "asc"));
  const docsSnap = await getDocs(q);
  const tickets: ITicket[] = [];

  docsSnap.forEach((docItem) => {
    tickets.push({
      id: docItem.id,
      bookingCode: docItem.data().bookingCode,
      ticketCode: docItem.data().ticketCode,
      eventName: docItem.data().eventName,
      status: docItem.data().status,
      dateUse: docItem.data().dateUse,
      dateExport: docItem.data().dateExport,
      gateCheck: docItem.data().gateCheck,
      typeTicket: docItem.data().typeTicket,
      isChecked: docItem.data().isChecked,
    });
  });

  return tickets;
});

export const getTicketsVerifyFilter = createAsyncThunk(
  "ticketVerify/getTicketsVerifyFilter",
  async (filter: FilterType) => {
    const ticketRef = collection(db, "ticket");
    const queryConstraints = [];

    if (filter.event !== "Tất cả") {
      queryConstraints.push(where("eventName", "==", filter.event));
    }
    if (typeof filter.isChecked !== "undefined") {
      queryConstraints.push(where("isChecked", "==", filter.isChecked));
    }
    if (filter.fromDate.isValid()) {
      queryConstraints.push(where("dateUse", ">=", filter.fromDate.toDate().toISOString()));
    }
    if (filter.toDate.isValid()) {
      queryConstraints.push(where("dateUse", "<=", filter.toDate.toDate().toISOString()));
    }

    const q = query(ticketRef, ...queryConstraints, orderBy("dateUse", "asc"));
    const tickets: ITicket[] = [];

    const docsSnap = await getDocs(q);
    docsSnap.forEach((docItem) => {
      tickets.push({
        id: docItem.id,
        bookingCode: docItem.data().bookingCode,
        ticketCode: docItem.data().ticketCode,
        eventName: docItem.data().eventName,
        status: docItem.data().status,
        dateUse: docItem.data().dateUse,
        dateExport: docItem.data().dateExport,
        gateCheck: docItem.data().gateCheck,
        typeTicket: docItem.data().typeTicket,
        isChecked: docItem.data().isChecked,
      });
    });

    return tickets;
  }
);

export const searchTicketsVerify = createAsyncThunk(
  "ticketVerify/searchTicketsVerify",
  async (searchString: string | undefined) => {
    const ticketRef = collection(db, "ticket");
    const queryConstraints = [];
    if (searchString !== "") {
      queryConstraints.push(where("ticketCode", ">=", searchString));
      queryConstraints.push(where("ticketCode", "<=", searchString + "\uf8ff"));
    } else {
      queryConstraints.push(orderBy("dateUse", "asc"));
    }
    const q = query(ticketRef, ...queryConstraints);
    const tickets: ITicket[] = [];

    const docsSnap = await getDocs(q);
    docsSnap.forEach((docItem) => {
      tickets.push({
        id: docItem.id,
        bookingCode: docItem.data().bookingCode,
        ticketCode: docItem.data().ticketCode,
        eventName: docItem.data().eventName,
        status: docItem.data().status,
        dateUse: docItem.data().dateUse,
        dateExport: docItem.data().dateExport,
        gateCheck: docItem.data().gateCheck,
        typeTicket: docItem.data().typeTicket,
        isChecked: docItem.data().isChecked,
      });
    });

    return tickets;
  }
);

export default ticketVerifySlice.reducer;
