import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import type { RootState } from "../configureStore";

interface IGateCheck {
  name: string;
  isCheck: boolean;
  gate: number;
}

// Define a type for the slice state
interface FilterModalState {
  statusChecked: string;
  fromDate: string;
  toDate: string;
  gateCheck: IGateCheck[];
}

// Define the initial state using that type
export const initialState: FilterModalState = {
  statusChecked: "Tất cả",
  fromDate: "",
  toDate: "",
  gateCheck: [
    { name: "Tất cả", isCheck: true, gate: 0 },
    { name: "Cổng 1", isCheck: false, gate: 1 },
    { name: "Cổng 2", isCheck: false, gate: 2 },
    { name: "Cổng 3", isCheck: false, gate: 3 },
    { name: "Cổng 4", isCheck: false, gate: 4 },
    { name: "Cổng 5", isCheck: false, gate: 5 },
  ],
};

export const filterModalSlide = createSlice({
  name: "filterModal",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    setStatusCheckedFilterModal: (state, action: PayloadAction<string>) => {
      state.statusChecked = action.payload;
    },
    setFromDateFilterModal: (state, action: PayloadAction<string>) => {
      state.fromDate = action.payload;
    },
    setToDateFilterModal: (state, action: PayloadAction<string>) => {
      state.toDate = action.payload;
    },
    setGateCheckFilterModal: (state, action: PayloadAction<IGateCheck[]>) => {
      state.gateCheck = action.payload;
    },
    resetFilterModal: () => initialState,
  },
});

export const {
  setStatusCheckedFilterModal,
  setFromDateFilterModal,
  setToDateFilterModal,
  setGateCheckFilterModal,
  resetFilterModal,
} = filterModalSlide.actions;

// // Other code such as selectors can use the imported `RootState` type
export const selectStatusCheckedFilterModal = (state: RootState) => state.filterModal.statusChecked;
export const selectFromDateFilterModal = (state: RootState) => state.filterModal.fromDate;
export const selectToDateFilterModal = (state: RootState) => state.filterModal.toDate;
export const selectGateCheckFilterModal = (state: RootState) => state.filterModal.gateCheck;

export default filterModalSlide.reducer;
