import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import type { RootState } from "../configureStore";
import {
  addDoc,
  collection,
  doc,
  getDoc,
  getDocs,
  query,
  updateDoc,
  where,
} from "firebase/firestore";
import { db } from "../../firebase/firebase";

// Define a type for the slice state
interface TicketPackageState {
  ticketPackages: ITicketPackage[];
  loading: boolean;
  currentPage: number;
}

interface IAddTicketPackage {
  dateApplied: string;
  dateExpired: string;
  eventCode: string;
  eventName: string;
  numTicketCombo: number;
  packageCode: string | undefined;
  packageName: string | undefined;
  packageStatus: string;
  priceCombo: number;
  priceTicket: number;
}

// Define the initial state using that type
const initialState: TicketPackageState = {
  ticketPackages: [],
  loading: false,
  currentPage: 1,
};

export const ticketPackageSlice = createSlice({
  name: "ticketPackage",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    setTicketPackages: (state, action: PayloadAction<ITicketPackage[]>) => {
      state.ticketPackages = action.payload;
    },
    setCurrentPagePackages: (state, action: PayloadAction<number>) => {
      state.currentPage = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getTicketPackages.pending, (state) => {
        state.loading = true;
      })
      .addCase(getTicketPackages.fulfilled, (state, action) => {
        state.ticketPackages = action.payload;
        state.currentPage = 1;
        state.loading = false;
      })
      .addCase(getTicketPackages.rejected, (state) => {
        state.ticketPackages = [];
        state.loading = false;
      })
      .addCase(searchTicketPackages.pending, (state) => {
        state.loading = true;
        state.currentPage = 1;
      })
      .addCase(searchTicketPackages.fulfilled, (state, action) => {
        state.ticketPackages = action.payload;
        state.loading = false;
      })
      .addCase(searchTicketPackages.rejected, (state) => {
        state.ticketPackages = [];
        state.loading = false;
      })
      .addCase(addPackageTicket.fulfilled, (state, action) => {
        state.ticketPackages.push(action.payload);
        state.loading = false;
      })
      .addCase(updatePackageTicket.fulfilled, (state, action) => {
        state.ticketPackages = state.ticketPackages.map((item) =>
          item.id === action.payload.id ? action.payload : item
        );
      });
  },
});

export const { setTicketPackages, setCurrentPagePackages } = ticketPackageSlice.actions;

// // Other code such as selectors can use the imported `RootState` type
export const selectTicketPackages = (state: RootState) => state.ticketPackage.ticketPackages;
export const selectLoadingTicketPackages = (state: RootState) => state.ticketPackage.loading;

export const getTicketPackages = createAsyncThunk("ticketPackage/getTicketPackages", async () => {
  const packageRef = collection(db, "package");
  const q = query(packageRef);
  const docsSnap = await getDocs(q);
  const packages: ITicketPackage[] = [];

  docsSnap.forEach((docItem) => {
    packages.push({
      id: docItem.id,
      packageCode: docItem.data().packageCode,
      packageName: docItem.data().packageName,
      dateApplied: docItem.data().dateApplied,
      dateExpired: docItem.data().dateExpired,
      priceTicket: docItem.data().priceTicket,
      priceCombo: docItem.data().priceCombo,
      numTicketCombo: docItem.data().numTicketCombo,
      packageStatus: docItem.data().packageStatus,
      eventCode: docItem.data().eventCode,
      eventName: docItem.data().eventName,
    });
  });

  return packages;
});

export const addPackageTicket = createAsyncThunk(
  "ticketPackage/addPackageTicket",
  async (packageTicket: IAddTicketPackage) => {
    const addNewDoc = await addDoc(collection(db, "package"), packageTicket);

    const docSnap = await getDoc(doc(db, "package", addNewDoc.id));

    const newPackage: ITicketPackage = {
      id: docSnap.id,
      packageCode: docSnap.data()?.packageCode,
      packageName: docSnap.data()?.packageName,
      dateApplied: docSnap.data()?.dateApplied,
      dateExpired: docSnap.data()?.dateExpired,
      priceTicket: docSnap.data()?.priceTicket,
      priceCombo: docSnap.data()?.priceCombo,
      numTicketCombo: docSnap.data()?.numTicketCombo,
      packageStatus: docSnap.data()?.packageStatus,
      eventCode: docSnap.data()?.eventCode,
      eventName: docSnap.data()?.eventName,
    };
    return newPackage;
  }
);

export const updatePackageTicket = createAsyncThunk(
  "ticketPackage/updatePackageTicket",
  async (updatedPackageTicket: ITicketPackage) => {
    await updateDoc(doc(db, "package", updatedPackageTicket.id), {
      packageCode: updatedPackageTicket.packageCode,
      packageName: updatedPackageTicket.packageName,
      dateApplied: updatedPackageTicket.dateApplied,
      dateExpired: updatedPackageTicket.dateExpired,
      priceTicket: updatedPackageTicket.priceTicket,
      priceCombo: updatedPackageTicket.priceCombo,
      numTicketCombo: updatedPackageTicket.numTicketCombo,
      packageStatus: updatedPackageTicket.packageStatus,
      eventCode: updatedPackageTicket.eventCode,
      eventName: updatedPackageTicket.eventName,
    });

    return updatedPackageTicket;
  }
);

export const searchTicketPackages = createAsyncThunk(
  "ticketPackage/searchTicketPackages",
  async (searchString: string | undefined) => {
    const packageRef = collection(db, "package");
    const queryConstraints = [];
    if (searchString !== "") {
      queryConstraints.push(where("packageCode", ">=", searchString));
      queryConstraints.push(where("packageCode", "<=", searchString + "\uf8ff"));
    }
    const q = query(packageRef, ...queryConstraints);
    const packages: ITicketPackage[] = [];

    const docsSnap = await getDocs(q);
    docsSnap.forEach((docItem) => {
      packages.push({
        id: docItem.id,
        packageCode: docItem.data().packageCode,
        packageName: docItem.data().packageName,
        dateApplied: docItem.data().dateApplied,
        dateExpired: docItem.data().dateExpired,
        priceTicket: docItem.data().priceTicket,
        priceCombo: docItem.data().priceCombo,
        numTicketCombo: docItem.data().numTicketCombo,
        packageStatus: docItem.data().packageStatus,
        eventCode: docItem.data().eventCode,
        eventName: docItem.data().eventName,
      });
    });

    return packages;
  }
);

export default ticketPackageSlice.reducer;
