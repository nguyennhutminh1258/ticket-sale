import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import type { RootState } from "../configureStore";
import dayjs from "dayjs";
import { collection, getCountFromServer, getDocs, query, where } from "firebase/firestore";
import { db } from "../../firebase/firebase";

interface TypePackageDoughnut {
  label: string;
  value: number;
}

interface DataChartDoughnut {
  name: string;
  data: TypePackageDoughnut[];
}

// Define a type for the slice state
interface ChartDoughnutState {
  dataChart: DataChartDoughnut[];
}

// Define the initial state using that type
const initialState: ChartDoughnutState = {
  dataChart: [],
};

export const chartDoughnutSlice = createSlice({
  name: "chartDoughnut",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    setDataChartDoughnut: (state, action: PayloadAction<DataChartDoughnut[]>) => {
      state.dataChart = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getDoughnutChart.fulfilled, (state, action) => {
      state.dataChart = action.payload;
    });
  },
});

export const { setDataChartDoughnut } = chartDoughnutSlice.actions;

// // Other code such as selectors can use the imported `RootState` type
export const selectDataChartDoughnut = (state: RootState) => state.chartDoughnut.dataChart;

export const getDoughnutChart = createAsyncThunk(
  "chartDoughnut/getDoughnutChart",
  async ({ month, year }: { month: number; year: number }) => {
    const firstDayOfMonth = dayjs().year(year).month(month).startOf("month");
    const lastDayOfMonth = dayjs().year(year).month(month).endOf("month");

    const ticketRef = collection(db, "ticket");
    const packageRef = collection(db, "package");

    const qPackage = query(packageRef);

    const statusTicket = ["Đã sử dụng", "Chưa sử dụng"];

    const docsSnapPackage = await getDocs(qPackage);
    const packages: ITicketPackage[] = [];
    docsSnapPackage.forEach((docItem) => {
      packages.push({
        id: docItem.id,
        packageCode: docItem.data().packageCode,
        packageName: docItem.data().packageName,
        dateApplied: docItem.data().dateApplied,
        dateExpired: docItem.data().dateExpired,
        priceTicket: docItem.data().priceTicket,
        priceCombo: docItem.data().priceCombo,
        numTicketCombo: docItem.data().numTicketCombo,
        packageStatus: docItem.data().packageStatus,
        eventCode: docItem.data().eventCode,
        eventName: docItem.data().eventName,
      });
    });

    const doughnutChartData = await Promise.all(
      packages.map(async (packageItem) => {
        const doughnutChartByPackageCode = await Promise.all(
          statusTicket.map(async (status) => {
            const qTicket = query(
              ticketRef,
              where("dateUse", ">=", firstDayOfMonth.toDate().toISOString()),
              where("dateUse", "<=", lastDayOfMonth.toDate().toISOString()),
              where("status", "==", status),
              where("bookingCode", "==", packageItem.packageCode)
            );
            const snapshot = await getCountFromServer(qTicket);

            return {
              label: status,
              value: snapshot.data().count,
            };
          })
        );

        return {
          name: packageItem.packageName,
          data: doughnutChartByPackageCode,
        };
      })
    );

    return doughnutChartData;
  }
);

export default chartDoughnutSlice.reducer;
