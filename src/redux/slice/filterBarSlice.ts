import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import type { RootState } from "../configureStore";

// Define a type for the slice state
interface FilterBarState {
  statusChecked: string;
  fromDate: string;
  toDate: string;
  event: string;
}

// Define the initial state using that type
const initialState: FilterBarState = {
  statusChecked: "All",
  fromDate: "",
  toDate: "",
  event: "Tất cả",
};

export const filterBarSlide = createSlice({
  name: "filterBar",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    setStatusCheckedFilterBar: (state, action: PayloadAction<string>) => {
      state.statusChecked = action.payload;
    },
    setFromDateFilterBar: (state, action: PayloadAction<string>) => {
      state.fromDate = action.payload;
    },
    setToDateFilterBar: (state, action: PayloadAction<string>) => {
      state.toDate = action.payload;
    },
    setEventFilterBar: (state, action: PayloadAction<string>) => {
      state.event = action.payload;
    },
    resetFilterBar: () => initialState,
  },
});

export const {
  setStatusCheckedFilterBar,
  setFromDateFilterBar,
  setToDateFilterBar,
  setEventFilterBar,
  resetFilterBar,
} = filterBarSlide.actions;

// // Other code such as selectors can use the imported `RootState` type
export const selectStatusCheckedFilterBar = (state: RootState) => state.filterBar.statusChecked;
export const selectFromDateFilterBar = (state: RootState) => state.filterBar.fromDate;
export const selectToDateFilterBar = (state: RootState) => state.filterBar.toDate;
export const selectEventFilterBar = (state: RootState) => state.filterBar.event;

export default filterBarSlide.reducer;
