import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import type { RootState } from "../configureStore";
import dayjs, { Dayjs } from "dayjs";
import { weekday } from "../../helper/calendar";
import { collection, getCountFromServer, getDocs, query, where } from "firebase/firestore";
import { db } from "../../firebase/firebase";

interface DataChartLine {
  day: string;
  value: number;
}

// Define a type for the slice state
interface ChartLineState {
  data: DataChartLine[];
  chartByWeek: boolean;
}

// Define the initial state using that type
const initialState: ChartLineState = {
  data: [],
  chartByWeek: false,
};

export const chartLineSlice = createSlice({
  name: "chartLine",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    setDataChartLine: (state, action: PayloadAction<DataChartLine[]>) => {
      state.data = action.payload;
    },
    setChartLineByWeek: (state, action: PayloadAction<boolean>) => {
      state.chartByWeek = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getLineChartByDay.fulfilled, (state, action) => {
        state.data = action.payload;
      })
      .addCase(getLineChartByWeek.fulfilled, (state, action) => {
        state.data = action.payload;
      });
  },
});

export const { setDataChartLine, setChartLineByWeek } = chartLineSlice.actions;

// // Other code such as selectors can use the imported `RootState` type
export const selectDataChartLine = (state: RootState) => state.chartLine.data;
export const selectChartLineByWeek = (state: RootState) => state.chartLine.chartByWeek;

export const getLineChartByDay = createAsyncThunk(
  "chartLine/getLineChartByDay",
  async ({ month, year }: { month: number; year: number }) => {
    console.log(month, year);
    const startWeek = dayjs().year(year).month(month).startOf("week");
    const endWeek = dayjs().year(year).month(month).endOf("week");
    const dayOfWeek: any = [];

    for (let i = startWeek.date(); i <= endWeek.date(); i++) {
      dayOfWeek.push(startWeek.date(i));
    }

    const ticketRef = collection(db, "ticket");
    const packageRef = collection(db, "package");

    const qPackage = query(packageRef);

    const docsSnapPackage = await getDocs(qPackage);
    const packages: ITicketPackage[] = [];
    docsSnapPackage.forEach((docItem) => {
      packages.push({
        id: docItem.id,
        packageCode: docItem.data().packageCode,
        packageName: docItem.data().packageName,
        dateApplied: docItem.data().dateApplied,
        dateExpired: docItem.data().dateExpired,
        priceTicket: docItem.data().priceTicket,
        priceCombo: docItem.data().priceCombo,
        numTicketCombo: docItem.data().numTicketCombo,
        packageStatus: docItem.data().packageStatus,
        eventCode: docItem.data().eventCode,
        eventName: docItem.data().eventName,
      });
    });

    const revenueData = await Promise.all(
      dayOfWeek.map(async (day: any) => {
        const revenueDay = await Promise.all(
          packages.map(async (packageItem) => {
            const qTicket = query(
              ticketRef,
              where("dateUse", "==", day.toDate().toISOString()),
              where("bookingCode", "==", packageItem.packageCode)
            );
            const snapshot = await getCountFromServer(qTicket);

            return {
              name: packageItem.packageName,
              totalPrice: snapshot.data().count * packageItem.priceTicket,
            };
          })
        );

        console.log(revenueDay);

        const total = revenueDay.reduce((result: number, re) => (result += re.totalPrice), 0);

        return {
          day: weekday[day.toDate().getDay()],
          value: total,
        };
      })
    );

    return revenueData;
  }
);

export const getLineChartByWeek = createAsyncThunk(
  "chartLine/getLineChartByWeek",
  async ({ month, year }: { month: number; year: number }) => {
    const firstDayOfMonth = dayjs().year(year).month(month).startOf("month");
    const lastDayOfMonth = dayjs().year(year).month(month).endOf("month");
    const weekRows = getDateOfMonth(firstDayOfMonth, lastDayOfMonth);

    const ticketRef = collection(db, "ticket");
    const packageRef = collection(db, "package");

    const qPackage = query(packageRef);

    const docsSnapPackage = await getDocs(qPackage);
    const packages: ITicketPackage[] = [];
    docsSnapPackage.forEach((docItem) => {
      packages.push({
        id: docItem.id,
        packageCode: docItem.data().packageCode,
        packageName: docItem.data().packageName,
        dateApplied: docItem.data().dateApplied,
        dateExpired: docItem.data().dateExpired,
        priceTicket: docItem.data().priceTicket,
        priceCombo: docItem.data().priceCombo,
        numTicketCombo: docItem.data().numTicketCombo,
        packageStatus: docItem.data().packageStatus,
        eventCode: docItem.data().eventCode,
        eventName: docItem.data().eventName,
      });
    });

    const revenueData = await Promise.all(
      weekRows.map(async (week: any) => {
        const firstDayOfWeek = week[0];
        const lastDayOfWeek = week[week.length - 1];
        const revenueDay = await Promise.all(
          packages.map(async (packageItem) => {
            const qTicket = query(
              ticketRef,
              where("dateUse", ">=", firstDayOfWeek.toDate().toISOString()),
              where("dateUse", "<=", lastDayOfWeek.toDate().toISOString()),
              where("bookingCode", "==", packageItem.packageCode)
            );
            const snapshot = await getCountFromServer(qTicket);

            return {
              name: packageItem.packageName,
              totalPrice: snapshot.data().count * packageItem.priceTicket,
            };
          })
        );

        const total = revenueDay.reduce((result: number, re) => (result += re.totalPrice), 0);

        return {
          day: firstDayOfWeek.format("DD/MM") + " - " + lastDayOfWeek.format("DD/MM"),
          value: total,
        };
      })
    );

    return revenueData;
  }
);

const getDateOfMonth = (firstDayOfMonth: Dayjs, lastDayOfMonth: Dayjs) => {
  const arrayOfDate: any = [];

  //create prefix date .day() => get day of week
  for (let i = 0; i < firstDayOfMonth.day(); i++) {
    arrayOfDate.push(firstDayOfMonth.day(i));
  }

  //generate current date .date() => get day of month
  for (let i = firstDayOfMonth.date(); i <= lastDayOfMonth.date(); i++) {
    arrayOfDate.push(firstDayOfMonth.date(i));
  }

  const remaining = 42 - arrayOfDate.length;
  for (let i = lastDayOfMonth.date() + 1; i < lastDayOfMonth.date() + remaining + 1; i++) {
    arrayOfDate.push(firstDayOfMonth.date(i));
  }

  const weekRows = [...Array(Math.ceil(arrayOfDate.length / 7))]?.map((row, index) =>
    arrayOfDate.slice(index * 7, index * 7 + 7)
  );

  return weekRows;
};

export default chartLineSlice.reducer;
