import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import type { RootState } from "../configureStore";

// Define a type for the slice state
interface ModalState {
  showFilterModal: boolean;
  showAddServiceModal: boolean;
  showUpdateServiceModal: boolean;
  showChangeDateModal: boolean;
}

// Define the initial state using that type
const initialState: ModalState = {
  showFilterModal: false,
  showAddServiceModal: false,
  showUpdateServiceModal: false,
  showChangeDateModal: false,
};

export const modalSlice = createSlice({
  name: "modal",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    setShowFilterModal: (state, action: PayloadAction<boolean>) => {
      state.showFilterModal = action.payload;
    },
    setShowAddServiceModal: (state, action: PayloadAction<boolean>) => {
      state.showAddServiceModal = action.payload;
    },
    setShowUpdateServiceModal: (state, action: PayloadAction<boolean>) => {
      state.showUpdateServiceModal = action.payload;
    },
    setShowChangeDateModal: (state, action: PayloadAction<boolean>) => {
      state.showChangeDateModal = action.payload;
    },
  },
});

export const {
  setShowFilterModal,
  setShowAddServiceModal,
  setShowUpdateServiceModal,
  setShowChangeDateModal,
} = modalSlice.actions;

// // Other code such as selectors can use the imported `RootState` type
export const selectShowFilterModal = (state: RootState) => state.modal.showFilterModal;
export const selectShowAddServiceModal = (state: RootState) => state.modal.showAddServiceModal;
export const selectShowUpdateServiceModal = (state: RootState) =>
  state.modal.showUpdateServiceModal;
export const selectShowChangeDateModal = (state: RootState) => state.modal.showChangeDateModal;

export default modalSlice.reducer;
