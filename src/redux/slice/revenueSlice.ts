import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import type { RootState } from "../configureStore";
import { collection, getCountFromServer, getDocs, query, where } from "firebase/firestore";
import { db } from "../../firebase/firebase";
import dayjs from "dayjs";

// Define a type for the slice state
interface RevenueState {
  revenue: number;
}

// Define the initial state using that type
const initialState: RevenueState = {
  revenue: 0,
};

export const revenueSlice = createSlice({
  name: "revenue",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    setRevenue: (state, action: PayloadAction<number>) => {
      state.revenue = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getRevenue.fulfilled, (state, action) => {
      state.revenue = action.payload;
    });
  },
});

export const { setRevenue } = revenueSlice.actions;

// // Other code such as selectors can use the imported `RootState` type
export const selectRevenue = (state: RootState) => state.revenue.revenue;

export const getRevenue = createAsyncThunk("revenue/getRevenue", async () => {
  const startWeek = dayjs().startOf("week");
  const endWeek = dayjs().endOf("week");

  const ticketRef = collection(db, "ticket");
  const packageRef = collection(db, "package");

  const qPackage = query(packageRef);

  const docsSnapPackage = await getDocs(qPackage);
  const packages: ITicketPackage[] = [];

  docsSnapPackage.forEach((docItem) => {
    packages.push({
      id: docItem.id,
      packageCode: docItem.data().packageCode,
      packageName: docItem.data().packageName,
      dateApplied: docItem.data().dateApplied,
      dateExpired: docItem.data().dateExpired,
      priceTicket: docItem.data().priceTicket,
      priceCombo: docItem.data().priceCombo,
      numTicketCombo: docItem.data().numTicketCombo,
      packageStatus: docItem.data().packageStatus,
      eventCode: docItem.data().eventCode,
      eventName: docItem.data().eventName,
    });
  });

  const revenue = await Promise.all(
    packages.map(async (packageItem) => {
      const qTicket = query(
        ticketRef,
        where("dateUse", ">=", startWeek.toDate().toISOString()),
        where("dateUse", "<=", endWeek.toDate().toISOString()),
        where("bookingCode", "==", packageItem.packageCode),
      );
      const snapshot = await getCountFromServer(qTicket);

      return {
        name: packageItem.packageName,
        totalPrice: snapshot.data().count * packageItem.priceTicket,
      };
    })
  );

  const total = revenue.reduce((result, { totalPrice }) => (result += totalPrice), 0);

  return total;
});

export default revenueSlice.reducer;
