import { combineReducers, configureStore } from "@reduxjs/toolkit";
import modalReducer from "./slice/modalSlice";
import revenueReducer from "./slice/revenueSlice";
import chartLineReducer from "./slice/chartLineSlice";
import chartDoughnutReducer from "./slice/chartDoughnutSlice";
import filterModalReducer from "./slice/filterModalSlice";
import filterBarReducer from "./slice/filterBarSlice";
import ticketVerifyReducer from "./slice/ticketVerifySlice";
import ticketManageReducer from "./slice/ticketManageSlice";
import ticketPackageReducer from "./slice/ticketPackageSlice";

const rootReducer = combineReducers({
  modal: modalReducer,
  revenue: revenueReducer,
  chartLine: chartLineReducer,
  chartDoughnut: chartDoughnutReducer,
  filterBar: filterBarReducer,
  filterModal: filterModalReducer,
  ticketManage: ticketManageReducer,
  ticketVerify: ticketVerifyReducer,
  ticketPackage: ticketPackageReducer,
});

export const store = configureStore({
  reducer: rootReducer,
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
