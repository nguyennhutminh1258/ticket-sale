import "./style.scss";
import ExportButton from "../../components/ExportButton";
import FilterButton from "../../components/FilterButton";
import TableManageTicket from "../../components/TableManageTicket";
import { AiOutlineSearch } from "react-icons/ai";
import FilterModal from "../../components/FilterModal";
import { useAppSelector } from "../../hooks/useAppSelector";
import { useAppDispatch } from "../../hooks/useAppDispatch";
import { selectShowFilterModal, setShowFilterModal } from "../../redux/slice/modalSlice";
import { ChangeEvent, useEffect, useState } from "react";
import {
  getTicketsManage,
  searchTicketsManage,
  selectTicketsManage,
} from "../../redux/slice/ticketManageSlice";
import { resetFilterModal } from "../../redux/slice/filterModalSlice";
import useIsFirstRender from "../../hooks/useIsFirstRender";

const ManageTicket = () => {
  const dispatch = useAppDispatch();
  const isFirst = useIsFirstRender();
  const showFilterModal = useAppSelector(selectShowFilterModal);

  const tickets = useAppSelector(selectTicketsManage);

  const [search, setSearch] = useState("");

  const headers = [
    { label: "STT", key: "stt" },
    { label: "Booking code", key: "bookingCode" },
    { label: "Số vé", key: "ticketCode" },
    { label: "Tên sự kiện", key: "eventName" },
    { label: "Tình trạng sử dụng", key: "status" },
    { label: "Ngày sử dụng", key: "dateUse" },
    { label: "Ngày xuất vé", key: "dateExport" },
    { label: "Cổng check - in", key: "gateCheck" },
  ];

  const exportData: {
    stt: number;
    bookingCode: string;
    ticketCode: string;
    eventName: string;
    status: string;
    dateUse: string;
    dateExport: string;
    gateCheck: string;
  }[] = [];

  tickets.map((ticket, index) =>
    exportData.push({
      stt: index + 1,
      bookingCode: ticket.bookingCode,
      ticketCode: ticket.ticketCode,
      eventName: ticket.eventName,
      status: ticket.status,
      dateUse: new Date(ticket.dateUse).toLocaleDateString("en-GB"),
      dateExport: new Date(ticket.dateExport).toLocaleDateString("en-GB"),
      gateCheck: "Cổng" + ticket.gateCheck.toString(),
    })
  );

  const handleHideFilterModal = () => {
    dispatch(setShowFilterModal(false));
    document.body.style.paddingRight = "0px";
    document.body.style.overflow = "unset";
  };

  const handleSearchTicket = (event: ChangeEvent<HTMLInputElement>) => {
    setSearch(event.target.value);
  };

  useEffect(() => {
    if (!isFirst && search !== "") {
      const handleSearchTicket = setTimeout(() => {
        dispatch(searchTicketsManage(search));
      }, 500);

      return () => clearTimeout(handleSearchTicket);
    } else {
      dispatch(getTicketsManage());
    }
  }, [dispatch, isFirst, search]);

  useEffect(() => {
    dispatch(resetFilterModal());
  }, [dispatch]);

  return (
    <>
      <div className="manage-ticket">
        <div className="manage-ticket__title">
          <span>Danh sách vé</span>
        </div>
        <div className="manage-ticket__extension">
          <div className="manage-ticket__search">
            <div className="search__input">
              <input
                type="text"
                placeholder="Tìm bằng số vé"
                value={search}
                onChange={(event) => handleSearchTicket(event)}
              />
            </div>
            <div className="search__icon">
              <AiOutlineSearch fontSize={24} />
            </div>
          </div>
          <div className="manage-ticket__button">
            <FilterButton />
            <ExportButton headers={headers} data={exportData} fileName={"quan-ly-ve.csv"} />
          </div>
        </div>
        <div className="manage-ticket__table">
          <TableManageTicket />
        </div>
      </div>
      {showFilterModal && (
        <div className="modal-filter">
          <div className="modal-filter--outside" onClick={handleHideFilterModal}></div>
          <div className="modal-filter--filter">
            <FilterModal />
          </div>
        </div>
      )}
    </>
  );
};

export default ManageTicket;
