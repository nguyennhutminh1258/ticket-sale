import "./style.scss";
import FilterBar from "../../components/FilterBar";
import TableCheckTicket from "../../components/TableVerifyTicket";
import ExportButton from "../../components/ExportButton";
import { AiOutlineSearch } from "react-icons/ai";
import { useAppDispatch } from "../../hooks/useAppDispatch";
import {
  getTicketsVerify,
  searchTicketsVerify,
  selectTicketsVerify,
} from "../../redux/slice/ticketVerifySlice";
import { ChangeEvent, useEffect, useState } from "react";
import { resetFilterBar } from "../../redux/slice/filterBarSlice";
import { useAppSelector } from "../../hooks/useAppSelector";
import useIsFirstRender from "../../hooks/useIsFirstRender";

const VerifyTicket = () => {
  const dispatch = useAppDispatch();
  const isFirst = useIsFirstRender();
  const [search, setSearch] = useState("");
  const tickets = useAppSelector(selectTicketsVerify);

  const headers = [
    { label: "STT", key: "stt" },
    { label: "Số vé", key: "ticketCode" },
    { label: "Ngày sử dụng", key: "dateUse" },
    { label: "Tên loại vé", key: "typeTicket" },
    { label: "Cổng check - in", key: "gateCheck" },
  ];

  const exportData: {
    stt: number;
    ticketCode: string;
    dateUse: string;
    typeTicket: string;
    gateCheck: string;
  }[] = [];

  tickets.map((ticket, index) =>
    exportData.push({
      stt: index + 1,
      ticketCode: ticket.ticketCode,
      dateUse: new Date(ticket.dateUse).toLocaleDateString("en-GB"),
      typeTicket: ticket.typeTicket,
      gateCheck: "Cổng" + ticket.gateCheck.toString(),
    })
  );

  const handleSearchTicket = (event: ChangeEvent<HTMLInputElement>) => {
    setSearch(event.target.value);
  };

  useEffect(() => {
    if (!isFirst && search !== "") {
      // limit request to server
      const handleSearchTicket = setTimeout(() => {
        dispatch(searchTicketsVerify(search));
      }, 500);

      return () => clearTimeout(handleSearchTicket);
    } else {
      dispatch(getTicketsVerify());
    }
  }, [dispatch, search, isFirst]);

  useEffect(() => {
    dispatch(resetFilterBar());
  }, [dispatch]);

  return (
    <div className="verify-ticket">
      <div className="verify-ticket__main">
        <div className="verify-ticket__title">
          <span>Đối soát vé</span>
        </div>
        <div className="verify-ticket__extension">
          <div className="verify-ticket__search">
            <div className="search__input">
              <input
                type="text"
                placeholder="Tìm bằng số vé"
                value={search}
                onChange={(event) => handleSearchTicket(event)}
              />
            </div>
            <div className="search__icon">
              <AiOutlineSearch fontSize={24} />
            </div>
          </div>
          <div className="verify-ticket__button">
            <ExportButton headers={headers} data={exportData} fileName={"doi-soat-ve.csv"} />
          </div>
        </div>
        <div className="verify-ticket__table">
          <TableCheckTicket />
        </div>
      </div>
      <div className="verify-ticket__filter">
        <FilterBar />
      </div>
    </div>
  );
};

export default VerifyTicket;
