import "./style.scss";
import Calendar from "../../components/Calendar";
import { useEffect, useRef, useState } from "react";
import { AiOutlineCalendar } from "react-icons/ai";
import ChartLine from "../../components/CharLine";
import ChartDoughnut from "../../components/ChartDoughnut";
import dayjs from "dayjs";
import { months } from "../../helper/calendar";
import { useAppDispatch } from "../../hooks/useAppDispatch";
import { getRevenue, selectRevenue } from "../../redux/slice/revenueSlice";
import { useAppSelector } from "../../hooks/useAppSelector";
import {
  getLineChartByDay,
  getLineChartByWeek,
  selectChartLineByWeek,
} from "../../redux/slice/chartLineSlice";
import { getDoughnutChart, selectDataChartDoughnut } from "../../redux/slice/chartDoughnutSlice";
import { useClickOutside } from "../../hooks/useClickOutside";

const Home = () => {
  const dispatch = useAppDispatch();
  const currentDate = dayjs();

  const revenue = useAppSelector(selectRevenue);
  const chartLineByWeek = useAppSelector(selectChartLineByWeek);
  const doughnutChartData = useAppSelector(selectDataChartDoughnut);

  const [selectedDayLine, setSelectedDayLine] = useState(currentDate);
  const [selectedDayDoughnut, setSelectedDayDoughnut] = useState(currentDate);

  const [showCalendarLine, setShowCalendarLine] = useState(false);
  const [showCalendarDoughnut, setShowCalendarDoughnut] = useState(false);

  const calendarLineRef = useRef<any>(null);
  const calendarDoughnutRef = useRef<any>(null);

  const handleClickCalendarLineOutside = () => {
    setShowCalendarLine(false);
  };
  const handleClickCalendarDoughnutOutside = () => {
    setShowCalendarDoughnut(false);
  };

  useClickOutside([calendarLineRef], handleClickCalendarLineOutside);
  useClickOutside([calendarDoughnutRef], handleClickCalendarDoughnutOutside);

  useEffect(() => {
    dispatch(getRevenue());
  }, [dispatch]);

  useEffect(() => {
    if (chartLineByWeek) {
      dispatch(
        getLineChartByWeek({ month: selectedDayLine.month(), year: selectedDayLine.year() })
      );
    } else {
      console.log("dispatch");
      dispatch(getLineChartByDay({ month: selectedDayLine.month(), year: selectedDayLine.year() }));
    }
  }, [chartLineByWeek, dispatch, selectedDayLine]);

  useEffect(() => {
    dispatch(
      getDoughnutChart({ month: selectedDayDoughnut.month(), year: selectedDayDoughnut.year() })
    );
  }, [dispatch, selectedDayDoughnut]);

  return (
    <div className="home">
      <div className="home__title">
        <span>Thống kê</span>
      </div>
      <div className="home__chart">
        <div className="home__chart-line">
          <div className="home__chart-line--top">
            <div className="text">
              <span>Doanh thu</span>
            </div>
            <div className="date-picker" ref={calendarLineRef}>
              <div className="date-picker__date">
                <span>
                  {months[selectedDayLine.month()]}, {selectedDayLine.year()}
                </span>
              </div>
              <div
                className="date-picker__icon"
                onClick={() => setShowCalendarLine(!showCalendarLine)}>
                <AiOutlineCalendar fontSize={24} color="#FF993C" />
              </div>
              {showCalendarLine && (
                <div className="calendar-menu">
                  <Calendar
                    selectedDay={selectedDayLine}
                    setSelectedDay={setSelectedDayLine}
                    chartLine={true}
                  />
                </div>
              )}
            </div>
          </div>
          <div className="home__chart-line--bottom">
            <ChartLine />
          </div>
        </div>
        <div className="home__total-income">
          <div className="home__total-income--title">
            <span>Tổng doanh thu theo tuần</span>
          </div>
          <div className="home__total-income--money">
            <span className="money">{new Intl.NumberFormat("en-DE").format(revenue)}</span>{" "}
            <span className="unit">đồng</span>
          </div>
        </div>
        <div className="home__chart-doughnut">
          <div className="home__chart-doughnut--date" ref={calendarDoughnutRef}>
            <div
              className="date-picker"
              onClick={() => setShowCalendarDoughnut(!showCalendarDoughnut)}>
              <div className="date-picker__date">
                <span>
                  {months[selectedDayDoughnut.month()]}, {selectedDayDoughnut.year()}
                </span>
              </div>
              <div className="date-picker__icon">
                <AiOutlineCalendar fontSize={24} color="#FF993C" />
              </div>
            </div>
            {showCalendarDoughnut && (
              <div className="calendar-menu">
                <Calendar
                  selectedDay={selectedDayDoughnut}
                  setSelectedDay={setSelectedDayDoughnut}
                />
              </div>
            )}
          </div>
          <div className="wrapper-chart">
            {doughnutChartData.map((doughnutChart) => {
              return (
                <div className="chart" key={doughnutChart.name}>
                  <div className="title">
                    <span>{doughnutChart.name}</span>
                  </div>
                  <ChartDoughnut dataChart={doughnutChart.data} />
                </div>
              );
            })}
          </div>
          <div className="legend">
            <div className="legend-item-1">
              <div className="color-1"></div>
              <div className="text-1">Vé đã sử dụng</div>
            </div>
            <div className="legend-item-2">
              <div className="color-2"></div>
              <div className="text-2">Vé chưa sử dụng</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
