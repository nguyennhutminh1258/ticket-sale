import Sidebar from "../../components/Sidebar";
import { Outlet } from "react-router-dom";
import "./style.scss";
import SearchComponent from "../../components/SearchComponent";
import Account from "../../components/Account";
// import FilterModal from "../../components/FilterModal";

const Layout = () => {
  return (
    <div className="layout">
      <div className="left">
        <div className="wrapper-sidebar">
          <Sidebar />
        </div>
        <div className="wrapper-copyright">
          <span>Copyright &copy; 2020 Alta Software</span>
        </div>
      </div>
      <div className="right">
        <div className="wrapper-header">
          <div className="wrapper-search">
            <SearchComponent />
          </div>
          <div className="wrapper-account">
            <Account />
          </div>
        </div>
        <div className="wrapper-main">
          <Outlet />
        </div>
      </div>
    </div>
  );
};

export default Layout;
