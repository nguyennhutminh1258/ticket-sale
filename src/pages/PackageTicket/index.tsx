import "./style.scss";
import { AiOutlineSearch } from "react-icons/ai";
import ExportButton from "../../components/ExportButton";
import AddPackageButton from "../../components/AddPackageButton";
import TablePackageTicket from "../../components/TablePackageTicket";
import AddPackageModal from "../../components/AddPackageModal";
import { useAppDispatch } from "../../hooks/useAppDispatch";
import { useAppSelector } from "../../hooks/useAppSelector";
import { selectShowAddServiceModal, setShowAddServiceModal } from "../../redux/slice/modalSlice";
import { ChangeEvent, useEffect, useState } from "react";
import {
  getTicketPackages,
  searchTicketPackages,
  selectTicketPackages,
} from "../../redux/slice/ticketPackageSlice";
import useIsFirstRender from "../../hooks/useIsFirstRender";

const PackageTicket = () => {
  const dispatch = useAppDispatch();
  const isFirst = useIsFirstRender();
  const packages = useAppSelector(selectTicketPackages);
  const showAddServiceModal = useAppSelector(selectShowAddServiceModal);
  const [search, setSearch] = useState("");

  const headers = [
    { label: "STT", key: "stt" },
    { label: "Mã gói", key: "packageCode" },
    { label: "Tên gói vé", key: "packageName" },
    { label: "Ngày áp dụng", key: "dateApplied" },
    { label: "Ngày hết hạn", key: "dateExpired" },
    { label: "Giá vé (VNĐ/Vé)", key: "priceTicket" },
    { label: "Giá Combo (VNĐ/Combo)", key: "priceCombo" },
    { label: "Tình trạng", key: "packageStatus" },
  ];

  const exportData: {
    stt: number;
    packageCode: string;
    packageName: string;
    dateApplied: string;
    dateExpired: string;
    priceTicket: string;
    priceCombo: string;
    packageStatus: string;
  }[] = [];

  packages.map((item, index) =>
    exportData.push({
      stt: index + 1,
      packageCode: item.packageCode,
      packageName: item.packageName,
      dateApplied:
        new Date(item.dateApplied).toLocaleDateString("en-GB") +
        " " +
        new Date(item.dateApplied).toLocaleTimeString("en-US", { hour12: false }),
      dateExpired:
        new Date(item.dateExpired).toLocaleDateString("en-GB") +
        " " +
        new Date(item.dateExpired).toLocaleTimeString("en-US", { hour12: false }),
      priceTicket: item.priceTicket + "VNĐ",
      priceCombo: item.priceCombo + "VNĐ/" + item.numTicketCombo + "Vé",
      packageStatus: item.packageStatus,
    })
  );

  const handleHideAddServiceModal = () => {
    dispatch(setShowAddServiceModal(false));
    document.body.style.paddingRight = "0px";
    document.body.style.overflow = "unset";
  };

  const handleSearchPackage = (event: ChangeEvent<HTMLInputElement>) => {
    setSearch(event.target.value);
  };

  useEffect(() => {
    if (!isFirst && search !== "") {
      // limit request to server
      const handleSearchTicket = setTimeout(() => {
        dispatch(searchTicketPackages(search));
      }, 500);

      return () => clearTimeout(handleSearchTicket);
    } else {
      dispatch(getTicketPackages());
    }
  }, [dispatch, isFirst, search]);

  return (
    <>
      <div className="service-ticket">
        <div className="service-ticket__title">
          <span>Danh sách gói vé</span>
        </div>
        <div className="service-ticket__extension">
          <div className="service-ticket__search">
            <div className="search__input">
              <input
                type="text"
                placeholder="Tìm bằng mã gói"
                value={search}
                onChange={(event) => handleSearchPackage(event)}
              />
            </div>
            <div className="search__icon">
              <AiOutlineSearch fontSize={24} />
            </div>
          </div>
          <div className="service-ticket__button">
            <ExportButton headers={headers} data={exportData} fileName={"goi-ve.csv"} />
            <AddPackageButton />
          </div>
        </div>
        <div className="service-ticket__table">
          <TablePackageTicket />
        </div>
      </div>
      {showAddServiceModal && (
        <div className="modal-add-service">
          <div className="modal-add-service--outside" onClick={handleHideAddServiceModal}></div>
          <div className="modal-add-service--add">
            <AddPackageModal />
          </div>
        </div>
      )}
    </>
  );
};

export default PackageTicket;
